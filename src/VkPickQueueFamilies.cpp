#include "vlk/VkPickQueueFamilies.hpp"
#include <vulkan/vulkan_core.h>
#include <cstdint>
#include <set>

namespace Vlk
{
	auto PickQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface, Hz::Span<VkQueueFamilyProperties const> queue_family_properties) -> QueueFamilies
	{
		QueueFamilies output;
		for (std::uint32_t i = 0; i < queue_family_properties.Size(); ++i)
		{
			auto const & queue_family = queue_family_properties[i];
			if (not output.present_family)
			{
				VkBool32 present_support = false;
				if (vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &present_support) == VK_SUCCESS)
				{
					if (present_support)
					{
						output.present_family = i;
					}
				}
			}
			if (not output.graphics_family)
			{
				if (queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT)
				{
					output.graphics_family = i;
				}
			}
			if (not output.compute_family)
			{
				if (queue_family.queueFlags & VK_QUEUE_COMPUTE_BIT)
				{
					output.compute_family = i;
				}
			}
			if (not output.transfer_family)
			{
				if (queue_family.queueFlags & VK_QUEUE_TRANSFER_BIT)
				{
					output.transfer_family = i;
				}
			}
		}
		return output;
	}

	bool operator<(DeviceQueue const & lhs, DeviceQueue const & rhs)
	{
		return lhs.index < rhs.index;
	}

    auto GetDeviceGraphicsQueue(VkDevice device, QueueFamilies const & queue_families) -> DeviceQueue
    {
		VkQueue output{};
        vkGetDeviceQueue(device, queue_families.graphics_family.value(), 0, &output);
		return { queue_families.graphics_family.value(), output };
    }

    auto GetDevicePresentQueue(VkDevice device, QueueFamilies const & queue_families) -> DeviceQueue
    {
		VkQueue output{};
        vkGetDeviceQueue(device, queue_families.present_family.value(), 0, &output);
		return { queue_families.present_family.value(), output };
    }

    auto GetDeviceComputeQueue(VkDevice device, QueueFamilies const & queue_families) -> DeviceQueue
	{
		VkQueue output{};
        vkGetDeviceQueue(device, queue_families.compute_family.value(), 0, &output);
		return { queue_families.compute_family.value(), output };
	}

    auto GetDeviceTransferQueue(VkDevice device, QueueFamilies const & queue_families) -> DeviceQueue
	{
		VkQueue output{};
        vkGetDeviceQueue(device, queue_families.transfer_family.value(), 0, &output);
		return { queue_families.transfer_family.value(), output };
	}

	auto GetUniqueQueues(Hz::Span<std::uint32_t const> device_queues) -> std::vector<std::uint32_t>
	{
		std::set<std::uint32_t> queues(device_queues.begin(), device_queues.end());
		return {queues.begin(), queues.end()};
	}

	auto GetQueueCreateInfo(Hz::Span<std::uint32_t const> device_queues, float const & priority) -> std::vector<VkDeviceQueueCreateInfo>
	{
		std::vector<VkDeviceQueueCreateInfo> output;
		for (auto const & family : device_queues)
		{
			output.push_back(VkDeviceQueueCreateInfo{
				.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
				.pNext = nullptr,
				.flags = 0,
				.queueFamilyIndex = family,
				.queueCount = 1,
				.pQueuePriorities = &priority,
			});
		}
		return output;
	}
}