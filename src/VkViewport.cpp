#include "vlk/VkViewport.hpp"

namespace Vlk
{
	auto GetViewport(float x, float y, float width, float height, float min_depth, float max_depth) -> VkViewport
	{
		return { x, y, width, height, min_depth, max_depth };
	}

	auto GetScissor(std::int32_t x, std::int32_t y, std::uint32_t width, std::uint32_t height) -> VkRect2D
	{
		return { {x, y}, {width, height} };
	}

	auto GetViewportCreateInfo(Hz::Span<VkViewport const> viewports, Hz::Span<VkRect2D const> scissors) -> VkPipelineViewportStateCreateInfo
	{
		return VkPipelineViewportStateCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.viewportCount = static_cast<std::uint32_t>(viewports.Size()),
			.pViewports = viewports.Data(),
			.scissorCount = static_cast<std::uint32_t>(scissors.Size()),
			.pScissors = scissors.Data(),
		};
	}
}