#include "vlk/VkPipeline.hpp"
#include <cstdint>
#include <stdexcept>
#include <cassert>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	auto GetPipelineInputAssemblyCreateInfo(VkPrimitiveTopology topology, bool primitive_restart_enable) -> VkPipelineInputAssemblyStateCreateInfo
	{
		return VkPipelineInputAssemblyStateCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
			.topology = topology,
			.primitiveRestartEnable = (primitive_restart_enable ? VK_TRUE : VK_FALSE),
		};
	}

	auto GetPipelineVertexInputCreateInfo(Hz::Span<VkVertexInputBindingDescription const> vertex_binding_descriptions, Hz::Span<VkVertexInputAttributeDescription const> vertex_attribute_descriptions) -> VkPipelineVertexInputStateCreateInfo
	{
		return {
			.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.vertexBindingDescriptionCount = static_cast<std::uint32_t>(vertex_binding_descriptions.Size()),
			.pVertexBindingDescriptions = vertex_binding_descriptions.Data(),
			.vertexAttributeDescriptionCount = static_cast<std::uint32_t>(vertex_attribute_descriptions.Size()),
			.pVertexAttributeDescriptions = vertex_attribute_descriptions.Data(),
		};
	}

	auto GetFillRasterizerCreateInfo(RasterizerCreateSettings create_settings) -> VkPipelineRasterizationStateCreateInfo
	{
		return {
			.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.depthClampEnable = (create_settings.depth_clamp_enable ? VK_TRUE : VK_FALSE),
			.rasterizerDiscardEnable = create_settings.discard_rasterizer_output ? VK_TRUE : VK_FALSE,
			.polygonMode = VK_POLYGON_MODE_FILL,
			.cullMode = static_cast<std::uint32_t>(
				(create_settings.cull_mode.cull_backface ? VK_CULL_MODE_BACK_BIT : VK_CULL_MODE_NONE) |
				(create_settings.cull_mode.cull_frontface ? VK_CULL_MODE_BACK_BIT : VK_CULL_MODE_NONE)
			),
			.frontFace = (create_settings.cull_mode.clockwise ? VK_FRONT_FACE_CLOCKWISE : VK_FRONT_FACE_COUNTER_CLOCKWISE),
			.depthBiasEnable = (create_settings.depth_bias.enabled ? VK_TRUE : VK_FALSE),
			.depthBiasConstantFactor = create_settings.depth_bias.constant_factor,
			.depthBiasClamp = create_settings.depth_bias.clamp,
			.depthBiasSlopeFactor = create_settings.depth_bias.slope_factor,
			.lineWidth = create_settings.line_width,
		};
	}

	auto GetDisabledMultisampleCreateInfo() -> VkPipelineMultisampleStateCreateInfo
	{
		return VkPipelineMultisampleStateCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
			.pNext = {},
			.flags = {},
			.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
			.sampleShadingEnable = VK_FALSE,
			.minSampleShading = 1.f,
			.pSampleMask = nullptr,
			.alphaToCoverageEnable = VK_FALSE,
			.alphaToOneEnable = VK_FALSE,
		};
	}

	auto GetDisabledDepthStencilCreateInfo() -> VkPipelineDepthStencilStateCreateInfo
	{
		return VkPipelineDepthStencilStateCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.depthTestEnable = VK_FALSE,
			.depthWriteEnable = VK_FALSE,
			.depthCompareOp = VK_COMPARE_OP_LESS,
			.depthBoundsTestEnable = VK_FALSE,
			.stencilTestEnable = VK_FALSE,
			.front = {},
			.back = {},
			.minDepthBounds = 0.f,
			.maxDepthBounds = 1.f,
		};
	}

	auto GetDisabledBlendAttachment() -> VkPipelineColorBlendAttachmentState 
	{
		return VkPipelineColorBlendAttachmentState{
			.blendEnable = VK_FALSE,
			.srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
			.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
			.colorBlendOp = VK_BLEND_OP_ADD,
			.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
			.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
			.alphaBlendOp = VK_BLEND_OP_ADD,
			.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
		};
	}

	auto GetAlphaBlendAttachment() -> VkPipelineColorBlendAttachmentState
	{
		return VkPipelineColorBlendAttachmentState{
			.blendEnable = VK_TRUE,
			.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
			.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
			.colorBlendOp = VK_BLEND_OP_ADD,
			.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
			.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
			.alphaBlendOp = VK_BLEND_OP_ADD,
			.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
		};
	}

	auto GetBlendCreateInfo(Hz::Span<VkPipelineColorBlendAttachmentState const> blend_attachments, BlendConstants blend_constants) -> VkPipelineColorBlendStateCreateInfo
	{
		return VkPipelineColorBlendStateCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.logicOpEnable = VK_FALSE,
			.logicOp = VK_LOGIC_OP_COPY,
			.attachmentCount = static_cast<std::uint32_t>(blend_attachments.Size()),
			.pAttachments = blend_attachments.Data(),
			.blendConstants = {
				blend_constants.r,
				blend_constants.g,
				blend_constants.b,
				blend_constants.a,
			},
		};
	}

	auto GetPipelineLayoutCreateInfo(Hz::Span<VkDescriptorSetLayout const> layouts, Hz::Span<VkPushConstantRange const> push_constants) -> VkPipelineLayoutCreateInfo
	{
		return VkPipelineLayoutCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
			.setLayoutCount = static_cast<std::uint32_t>(layouts.Size()),
			.pSetLayouts = layouts.Data(),
			.pushConstantRangeCount = static_cast<std::uint32_t>(push_constants.Size()),
			.pPushConstantRanges = push_constants.Data(),
		};
	}

	auto CreatePipelineLayout(VkDevice device, VkPipelineLayoutCreateInfo const & create_info) -> PipelineLayout
	{
		PipelineLayout output(device, nullptr);
		if (vkCreatePipelineLayout(device, &create_info, nullptr, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create pipeline layout!");
		}
		return output;
	}

	auto CreatePipelineLayout( VkDevice device, Hz::Span<VkDescriptorSetLayout const> layouts, Hz::Span<VkPushConstantRange const> push_constants ) -> PipelineLayout
	{
		return CreatePipelineLayout(device, GetPipelineLayoutCreateInfo(layouts, push_constants));
	}

	auto GetMinimalColorAttachmentDescription(VkFormat format) -> VkAttachmentDescription
	{
		return VkAttachmentDescription{
			.flags = {},
			.format = format,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
		};
	}

	auto GetAttachmentReference(std::uint32_t attachment, VkImageLayout layout) -> VkAttachmentReference
	{
		return VkAttachmentReference{
			.attachment = attachment,
			.layout = layout
		};
	}

	auto GetSubpassDescription(SubpassDescriptionArgs args) -> VkSubpassDescription
	{
		assert((not args.resolve_attachments.Data() or args.resolve_attachments.Size() == args.colour_attachments.Size()));
		assert((not args.depth_stencil_attachments.Data() or args.depth_stencil_attachments.Size() == args.colour_attachments.Size()));
		return VkSubpassDescription{
			.flags = {},
			.pipelineBindPoint = {},
			.inputAttachmentCount = static_cast<std::uint32_t>(args.input_attachments.Size()),
			.pInputAttachments = args.input_attachments.Data(),
			.colorAttachmentCount = static_cast<std::uint32_t>(args.colour_attachments.Size()),
			.pColorAttachments = args.colour_attachments.Data(),
			.pResolveAttachments = args.resolve_attachments.Data(),
			.pDepthStencilAttachment = args.depth_stencil_attachments.Data(),
			.preserveAttachmentCount = static_cast<std::uint32_t>(args.preserve_attachments.Size()),
			.pPreserveAttachments = args.preserve_attachments.Data(),
		};
	}

	auto GetSubpassDependency(std::uint32_t src_subpass, std::uint32_t dst_subpass) -> VkSubpassDependency
	{
		return VkSubpassDependency
		{
			.srcSubpass = src_subpass,
			.dstSubpass = dst_subpass,
			.srcStageMask = 0,
			.dstStageMask = 0,
			.srcAccessMask = 0,
			.dstAccessMask = 0,
			.dependencyFlags = 0,
		};
	}

	auto GetRenderPassCreateInfo(RenderPassArgs args) -> VkRenderPassCreateInfo
	{
		return VkRenderPassCreateInfo{
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.attachmentCount = static_cast<std::uint32_t>(args.attachments.Size()),
			.pAttachments = args.attachments.Data(),
			.subpassCount = static_cast<std::uint32_t>(args.subpasses.Size()),
			.pSubpasses = args.subpasses.Data(),
			.dependencyCount = static_cast<std::uint32_t>(args.dependencies.Size()),
			.pDependencies = args.dependencies.Data(),
		};
	}

	auto CreateRenderPass(VkDevice device, VkRenderPassCreateInfo const & create_info) -> RenderPass
	{
		RenderPass output(device, nullptr);
		if (vkCreateRenderPass(device, &create_info, nullptr, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create render pass!");
		}
		return output;
	}

	auto CreateRenderPass(VkDevice device, RenderPassArgs args) -> RenderPass
	{
		return CreateRenderPass(device, GetRenderPassCreateInfo(args));
	}


	auto GetPipelineCreateInfo(PipelineArgs args) -> VkGraphicsPipelineCreateInfo
	{
		return VkGraphicsPipelineCreateInfo{
			.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.stageCount = static_cast<std::uint32_t>(args.stages.Size()),
			.pStages = args.stages.Data(),
			.pVertexInputState = args.vertex_input_state,
			.pInputAssemblyState = args.input_assembly_state,
			.pTessellationState = args.tessellation_state,
			.pViewportState = args.viewport_state,
			.pRasterizationState = args.rasterization_state,
			.pMultisampleState = args.multisample_state,
			.pDepthStencilState = args.depth_stencil_state,
			.pColorBlendState = args.color_blend_state,
			.pDynamicState = args.dynamic_state,
			.layout = args.layout,
			.renderPass = args.render_pass,
			.subpass = args.subpass,
			.basePipelineHandle = VK_NULL_HANDLE,
			.basePipelineIndex = -1,
		};
	}

	auto CreatePipeline(VkDevice device, Hz::Span<VkGraphicsPipelineCreateInfo const> create_infos) -> Pipeline 
	{
		Pipeline output(device, nullptr);
		if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, static_cast<std::uint32_t>(create_infos.Size()), create_infos.Data(), nullptr, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create graphics pipeline!");
		}
		return output;
	}
}