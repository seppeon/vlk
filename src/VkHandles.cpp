#include "vlk/VkHandles.hpp"
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	CommandBuffers::CommandBuffers(VkDevice device, VkCommandPool command_pool, std::size_t buffer_count)
		: m_device(device)
		, m_command_pool(command_pool)
		, m_buffers(buffer_count)
	{}
	CommandBuffers::CommandBuffers(CommandBuffers && command_buffers)
		: m_device(std::exchange(command_buffers.m_device, nullptr))
		, m_command_pool(command_buffers.m_command_pool)
		, m_buffers(std::move(command_buffers.m_buffers))
	{}
	CommandBuffers& CommandBuffers::operator=(CommandBuffers && command_buffers)
	{
		if (this == &command_buffers) return *this;
		m_device = std::exchange(command_buffers.m_device, nullptr);
		m_command_pool = command_buffers.m_command_pool;
		m_buffers = std::move(command_buffers.m_buffers);
		return *this;
	}

	auto CommandBuffers::data() noexcept -> VkCommandBuffer *
	{
		return m_buffers.data();
	}

	auto CommandBuffers::data() const noexcept -> VkCommandBuffer const *
	{
		return m_buffers.data();
	}

	auto CommandBuffers::size() const noexcept -> std::uint32_t
	{
		return static_cast<std::uint32_t>(m_buffers.size());
	}

	auto CommandBuffers::front() const noexcept -> VkCommandBuffer
	{
		return m_buffers.front();
	}

	auto CommandBuffers::back() const noexcept -> VkCommandBuffer
	{
		return m_buffers.back();
	}

	CommandBuffers::~CommandBuffers()
	{
		if (m_device)
		{
			vkFreeCommandBuffers(m_device, m_command_pool, size(), data());
		}
	}

	DescriptorSets::DescriptorSets(VkDevice logical_device, VkDescriptorPool descriptor_pool, std::size_t descriptor_set_count, VkAllocationCallbacks const * allocator)
		: m_device(logical_device)
		, m_descriptor_pool(descriptor_pool)
		, m_allocator(allocator)
		, m_descriptor_sets(descriptor_set_count)
	{}
	DescriptorSets::DescriptorSets(DescriptorSets && descriptor_sets)
		: m_device(std::exchange(descriptor_sets.m_device, nullptr))
		, m_descriptor_pool(descriptor_sets.m_descriptor_pool)
		, m_allocator(descriptor_sets.m_allocator)
		, m_descriptor_sets(std::move(descriptor_sets.m_descriptor_sets))
	{}
	DescriptorSets& DescriptorSets::operator=(DescriptorSets && descriptor_sets)
	{
		if (this == &descriptor_sets) return *this;
		m_device = std::exchange(descriptor_sets.m_device, nullptr);
		m_descriptor_pool = descriptor_sets.m_descriptor_pool;
		m_allocator = descriptor_sets.m_allocator;
		m_descriptor_sets = std::move(descriptor_sets.m_descriptor_sets);
		return *this;
	}
	DescriptorSets::~DescriptorSets()
	{
		if (m_device)
		{
			vkFreeDescriptorSets(m_device, m_descriptor_pool, static_cast<std::uint32_t>(m_descriptor_sets.size()), m_descriptor_sets.data());
		}
	}

	auto DescriptorSets::data() noexcept -> VkDescriptorSet *
	{
		return m_descriptor_sets.data();
	}
	auto DescriptorSets::data() const noexcept -> VkDescriptorSet const *
	{
		return m_descriptor_sets.data();
	}
	auto DescriptorSets::size() const noexcept -> std::uint32_t
	{
		return m_descriptor_sets.size();
	}
	auto DescriptorSets::front() const noexcept -> VkDescriptorSet
	{
		return m_descriptor_sets.front();
	}
	auto DescriptorSets::back() const noexcept -> VkDescriptorSet
	{
		return m_descriptor_sets.back();
	}
	auto DescriptorSets::begin() const noexcept -> VkDescriptorSet const *
	{
		return m_descriptor_sets.data();
	}
	auto DescriptorSets::end() const noexcept -> VkDescriptorSet const *
	{
		return m_descriptor_sets.data() + m_descriptor_sets.size();
	}
	auto DescriptorSets::operator[](std::size_t i) const noexcept -> VkDescriptorSet
	{
		return m_descriptor_sets[i];
	}
}