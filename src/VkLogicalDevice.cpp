#include "vlk/VkLogicalDevice.hpp"
#include "vlk/VkPickQueueFamilies.hpp"
#include "vlk/VkUtil.hpp"
#include <cstdint>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	auto CreateLogicalDevice(VkPhysicalDevice const & physical_device, std::set<std::string> const & extensions, Hz::Span<VkDeviceQueueCreateInfo const> queue_families) -> Device
	{
		auto extensions_temp = ToCStrVector(extensions);
		VkPhysicalDeviceFeatures device_features{
		};
		VkDeviceCreateInfo create_info{
			.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.queueCreateInfoCount = static_cast<std::uint32_t>(queue_families.Size()),
			.pQueueCreateInfos = queue_families.Data(),
			.enabledLayerCount = {},
			.ppEnabledLayerNames = {},
			.enabledExtensionCount = static_cast<std::uint32_t>(extensions_temp.size()),
			.ppEnabledExtensionNames = extensions_temp.data(),
			.pEnabledFeatures = &device_features,
		};
		Device output(nullptr);
		if (vkCreateDevice(physical_device, &create_info, nullptr, &output) != VK_SUCCESS) {
			throw std::runtime_error("couldn't create a physical device.");
		}
		return output;
	}

	auto CreateLogicalDevice(VkPhysicalDevice const & physical_device, std::set<std::string> const & extensions, Hz::Span<std::uint32_t const> device_queues, float const & priority) -> Device
	{
		return CreateLogicalDevice(physical_device, extensions, GetQueueCreateInfo(device_queues, priority));
	}
}