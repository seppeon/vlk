#include "vlk/VkAllocate.hpp"
#include <cstdint>
#include <utility>
#include <stdexcept>

namespace Vlk
{
	MappedMemory::MappedMemory(VkDevice device, VkDeviceMemory memory, VkDeviceSize offset, VkDeviceSize length, VkMemoryMapFlags flags)
		: m_device( device )
		, m_memory( memory )
	{
		if (vkMapMemory(m_device, m_memory, offset, length, flags, &m_ptr) != VK_SUCCESS)
		{
			throw std::runtime_error("Unable to mapped memory.");
		}
	}
	MappedMemory::MappedMemory(MappedMemory && mapped_memory)
		: m_device( std::exchange(mapped_memory.m_device, nullptr) )
		, m_memory( std::exchange(mapped_memory.m_memory, nullptr) )
		, m_ptr( std::exchange(mapped_memory.m_ptr, nullptr) )
	{}
	MappedMemory& MappedMemory::operator=(MappedMemory && mapped_memory)
	{
		if (this == &mapped_memory) return *this;
		m_device = std::exchange(mapped_memory.m_device, nullptr);
		m_memory = std::exchange(mapped_memory.m_memory, nullptr);
		m_ptr = std::exchange(mapped_memory.m_ptr, nullptr);
		return *this;
	}
	MappedMemory::~MappedMemory()
	{
		if (m_ptr != nullptr) { vkUnmapMemory(m_device, m_memory); }
	}
	auto MappedMemory::Get() const noexcept -> void *
	{
		return m_ptr;
	}

	DeviceMemory::DeviceMemory(VkDevice device, std::size_t alloc_size, std::size_t type_index, VkAllocationCallbacks const * allocator)
		: m_device(device)
		, m_allocator(allocator)
	{
		auto const create_info = VkMemoryAllocateInfo{
			.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
			.pNext = {},
			.allocationSize = static_cast<std::uint32_t>(alloc_size),
			.memoryTypeIndex = static_cast<std::uint32_t>(type_index),
		};
		if (vkAllocateMemory(m_device, &create_info, m_allocator, &m_memory) != VK_SUCCESS)
		{
			throw std::runtime_error("Unable to allocate memory.");
		}
	}
	DeviceMemory::DeviceMemory(DeviceMemory && device_memory)
		: m_device( std::exchange(device_memory.m_device, nullptr) )
		, m_memory( std::exchange(device_memory.m_memory, nullptr) )
		, m_allocator( std::exchange(device_memory.m_allocator, nullptr) )
	{}
	DeviceMemory& DeviceMemory::operator=(DeviceMemory && device_memory)
	{
		if (this == &device_memory) return *this;
		m_device = std::exchange(device_memory.m_device, nullptr);
		m_memory = std::exchange(device_memory.m_memory, nullptr);
		m_allocator = std::exchange(device_memory.m_allocator, nullptr);
		return *this;
	}
	DeviceMemory::~DeviceMemory()
	{
		if (m_device != nullptr)
		{
			vkFreeMemory(m_device, m_memory, m_allocator);
		}
	}
	auto BindBuffer(DeviceMemory const & device_memory, VkBuffer buffer, std::size_t offset) noexcept -> VkResult
	{
		return vkBindBufferMemory(device_memory.m_device, buffer, device_memory.m_memory, offset);
	}
	auto BindImage(DeviceMemory const & device_memory, VkImage image, std::size_t offset) noexcept -> VkResult
	{
		return vkBindImageMemory(device_memory.m_device, image, device_memory.m_memory, offset);
	}
	auto DeviceMemory::MapMemory(std::size_t offset, std::size_t length, VkMemoryMapFlags flags) const noexcept -> MappedMemory
	{
		return MappedMemory( m_device, m_memory, offset, length, flags );
	}
}