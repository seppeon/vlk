#include "vlk/VkInstanceLayers.hpp"
#include <cstdint>
#include <iostream>
#include <stdexcept>

namespace Vlk
{
    auto GetInstanceLayersCount() -> std::size_t
    {
        std::uint32_t output = 0;
        if (vkEnumerateInstanceLayerProperties(&output, nullptr) != VK_SUCCESS)
        {
            throw std::runtime_error("vulkan: Unable to get the instance layer count.");
        }
        return output;
    }

    auto GetInstanceLayers(std::size_t layer_count) -> std::vector<VkLayerProperties>
    {
        std::uint32_t count = layer_count;
        std::vector<VkLayerProperties> output(layer_count);
        auto const result = vkEnumerateInstanceLayerProperties(&count, output.data());
        if (result != VK_SUCCESS)
        {
            throw std::runtime_error("vulkan: Unable to get the instance layers.");
        }
        output.resize(count);
        return output;
    }

    auto HasInstanceLayer(std::string name, std::vector<VkLayerProperties> const & layers) -> bool
    {
        for (auto const & layer : layers)
        {
            if (name == layer.layerName)
            {
                return true;
            }
        }
        return false;
    }
}