#include "vlk/VkPickPhysicalDevice.hpp"
#include <vulkan/vulkan.hpp>
#include <algorithm>
#include <cstdint>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
    namespace
    {
        auto DeviceNotSuitable(VkPhysicalDevice const * device) -> bool
        {
            VkPhysicalDeviceFeatures deviceFeatures{};
            vkGetPhysicalDeviceFeatures(*device, &deviceFeatures);
            return not deviceFeatures.geometryShader;
        }

        auto RankDevice(VkPhysicalDevice device) -> std::int32_t
        {
            std::int32_t score = 0;
            VkPhysicalDeviceProperties deviceProperties{};
            vkGetPhysicalDeviceProperties(device, &deviceProperties);
            if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
            {
                score += 1000;
            }
            score += deviceProperties.limits.maxImageDimension2D;
            return score;
        }

        constexpr VkPresentModeKHR ideal_present_mode = VK_PRESENT_MODE_MAILBOX_KHR;

        constexpr VkSurfaceFormatKHR ideal_surface_format = {
            .format = VK_FORMAT_B8G8R8A8_SRGB,
            .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
        };
    }

    auto PickPhysicalDevice(Hz::Span<VkPhysicalDevice const> devices) -> VkPhysicalDevice const *
    {
        std::vector<VkPhysicalDevice const *> device_set;
        device_set.reserve(devices.Size());
        for (auto & device : devices) { device_set.push_back(&device); } 
        std::erase_if(device_set, &DeviceNotSuitable);
        if (device_set.empty()) return nullptr;
        return *std::max_element(device_set.begin(), device_set.end(), [](VkPhysicalDevice const * lhs, VkPhysicalDevice const * rhs)
        {
            return RankDevice(*lhs) < RankDevice(*rhs);
        });
    }

    auto PickPresentMode(SwapChainSupportDetails const & support_details) -> VkPresentModeKHR
    {
        if (support_details.HasPresentMode(ideal_present_mode))
        {
            return ideal_present_mode;
        }
        return VK_PRESENT_MODE_FIFO_KHR;
    }

    auto PickSurfaceFormat(SwapChainSupportDetails const & support_details) -> VkSurfaceFormatKHR
    {
        if (support_details.HasSurfaceFormat(ideal_surface_format))
        {
            return ideal_surface_format;
        }
        return support_details.formats.front();
    }

    auto PickSwapExtents(SwapChainSupportDetails const & support_details, std::uint32_t x_pixels, std::uint32_t y_pixels) -> VkExtent2D
    {
        auto const & capabilities = support_details.capabilities;
        if (capabilities.currentExtent.width != ~uint32_t())
        {
            return capabilities.currentExtent;
        }
        else
        {
            auto const & min_extent = capabilities.minImageExtent;
            auto const & max_extent = capabilities.maxImageExtent;
            return VkExtent2D{
                std::clamp(x_pixels, min_extent.width, max_extent.width),
                std::clamp(y_pixels, min_extent.height, max_extent.height)
            };
        }
    }

    auto PickImageCount(SwapChainSupportDetails const & support_details, std::uint32_t desired_extra_images_than_minimum) -> std::uint32_t
    {
        auto const & capabilities = support_details.capabilities;
        auto const & min_image_count = capabilities.minImageCount;
        auto const & max_image_count = capabilities.maxImageCount;
        return std::min(min_image_count + desired_extra_images_than_minimum, max_image_count);
    }
}