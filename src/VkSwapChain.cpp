#include "vlk/VkSwapChain.hpp"
#include <stdexcept>
#include <cassert>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	auto CreateSwapChain(VkDevice device, VkSurfaceKHR surface, Hz::Span<std::uint32_t const> queue_families, VkSurfaceCapabilitiesKHR capibilities, VkSurfaceFormatKHR surface_format, VkPresentModeKHR present_mode, VkExtent2D extent, std::uint32_t image_count) -> SwapChain
	{
		auto const create_info = VkSwapchainCreateInfoKHR{
			.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
			.pNext = nullptr,
			.flags = {},
			.surface = surface,
			.minImageCount = image_count,
			.imageFormat = surface_format.format,
			.imageColorSpace = surface_format.colorSpace,
			.imageExtent = extent,
			.imageArrayLayers = 1,
			.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
			.imageSharingMode = {},
			.queueFamilyIndexCount = static_cast<std::uint32_t>(queue_families.Size()),
			.pQueueFamilyIndices = queue_families.Data(),
			.preTransform = capibilities.currentTransform,
			.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			.presentMode = present_mode,
			.clipped = VK_TRUE,
			.oldSwapchain = nullptr,
		};
		SwapChain output(device, nullptr);
		if (vkCreateSwapchainKHR(device, &create_info, nullptr, &output) != VK_SUCCESS) {
			throw std::runtime_error("unable to create swapchain.");
		}
		return output;
	}

	namespace
	{
		auto GetSwapchainImageCount(VkDevice device, VkSwapchainKHR swap_chain) -> std::uint32_t
		{
			std::uint32_t image_count = 0;
			if (vkGetSwapchainImagesKHR(device, swap_chain, &image_count, nullptr) != VK_SUCCESS)
			{
				throw std::runtime_error("unable to get swapchain image count.");
			}
			return image_count;
		}
	}

	auto CreateSwapchainImages(VkDevice device, VkSwapchainKHR swap_chain) -> std::vector<VkImage>
	{
		std::uint32_t image_count = GetSwapchainImageCount(device, swap_chain);
		std::vector<VkImage> output(image_count);
		if (vkGetSwapchainImagesKHR(device, swap_chain, &image_count, output.data()) != VK_SUCCESS)
		{
			throw std::runtime_error("unable to get swapchain images.");
		}
		return output;
	}

	auto CreateSwapchainImageViews(VkDevice device, Hz::Span<const VkImage> images, VkFormat swap_chain_format) -> std::vector<ImageView>
	{
		std::vector<ImageView> output;
		output.reserve(images.Size());
		for (std::size_t i = 0; i < images.Size(); ++i)
		{
			auto const create_info = VkImageViewCreateInfo{
				.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
				.pNext = nullptr,
				.image = images[i],
				.viewType = VK_IMAGE_VIEW_TYPE_2D,
				.format = swap_chain_format,
				.components = {
					.r = VK_COMPONENT_SWIZZLE_IDENTITY,
					.g = VK_COMPONENT_SWIZZLE_IDENTITY,
					.b = VK_COMPONENT_SWIZZLE_IDENTITY,
					.a = VK_COMPONENT_SWIZZLE_IDENTITY,
				},
				.subresourceRange
				{
					.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
					.baseMipLevel = 0,
					.levelCount = 1,
					.baseArrayLayer = 0,
					.layerCount = 1,
				},
			};
			if (vkCreateImageView(device, &create_info, nullptr, &output.emplace_back(ImageView(device, nullptr))) != VK_SUCCESS)
			{
				throw std::runtime_error("failed to create image views.");
			}
		}
		return output;
	}
}