#include "vlk/VkQueue.hpp"
#include <cassert>
#include <cstdint>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	void QueueSubmit(VkQueue queue, Hz::Span<VkSubmitInfo const> submit_infos, VkFence signalled_fence)
	{
		if (vkQueueSubmit(queue, static_cast<std::uint32_t>(submit_infos.Size()), submit_infos.Data(), signalled_fence) != VK_SUCCESS)
		{
			throw std::runtime_error("Unable to submit queue.");
		}
	}

	void QueuePresent(VkQueue queue, Hz::Span<VkSemaphore const> wait_semaphores, Hz::Span<VkSwapchainKHR const> swap_chains, Hz::Span<std::uint32_t const> swap_indicies, Hz::Span<VkResult> results)
	{
		assert(swap_chains.Size() == swap_indicies.Size());
		assert((results.Empty() or swap_chains.Size() == results.Size()));
		auto const present_info = VkPresentInfoKHR{
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.pNext = nullptr,
			.waitSemaphoreCount = static_cast<std::uint32_t>(wait_semaphores.Size()),
			.pWaitSemaphores = wait_semaphores.Data(),
			.swapchainCount = static_cast<std::uint32_t>(swap_chains.Size()),
			.pSwapchains = swap_chains.Data(),
			.pImageIndices = swap_indicies.Data(),
			.pResults = results.Data(),
		};
		if (vkQueuePresentKHR( queue, &present_info ) != VK_SUCCESS)
		{
			throw std::runtime_error("Unable to present queue.");
		}
	}
}