#include "vlk/VkVersion.hpp"
#include <vulkan/vulkan.h>

namespace Vlk
{
    namespace
    {
        auto constexpr vk_api_major_minor_patch = VK_API_VERSION_1_3;
    }

    auto ApiVersion() -> FL::Semver
    {
        return FL::Semver{
            .major = VK_VERSION_MAJOR(vk_api_major_minor_patch),
            .minor = VK_VERSION_MINOR(vk_api_major_minor_patch),
            .patch = VK_VERSION_PATCH(vk_api_major_minor_patch) 
        };
    }

    auto ToVkVersion(FL::Semver const & semver) -> std::uint32_t
    {
        return VK_MAKE_API_VERSION(0, semver.major, semver.minor, semver.patch);
    }
}