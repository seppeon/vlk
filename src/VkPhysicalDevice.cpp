#include "vlk/VkPhysicalDevice.hpp"
#include <stdexcept>
#include <vulkan/vulkan.h>
#include <cstdint>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
    namespace
    {
        auto GetPhysicalDeviceCount(VkInstance instance) -> std::uint32_t
        {
            std::uint32_t output = 0;
            if (vkEnumeratePhysicalDevices(instance, &output, nullptr) != VK_SUCCESS)
            {
                throw std::runtime_error("Could not get the number of physical devices (graphics cards).");
            }
            return output;
        }
        auto GetPhysicalDeviceSupportedExtensionNameCount(VkPhysicalDevice device) -> std::uint32_t
        {
            std::uint32_t output = 0;
            if (vkEnumerateDeviceExtensionProperties(device, nullptr, &output, nullptr) != VK_SUCCESS)
            {
                throw std::runtime_error("Could not get the number of physical devices(graphics card) extensions.");
            }
            return output;
        }
    }

    auto GetPhysicalDevices(VkInstance instance) -> std::vector<VkPhysicalDevice>
    {
        auto device_count = GetPhysicalDeviceCount(instance);
        std::vector<VkPhysicalDevice> devices(device_count);
        if (vkEnumeratePhysicalDevices(instance, &device_count, devices.data()) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not get the physical devices (graphics cards).");
        }
        devices.resize(device_count);
        return devices;
    }

    auto GetPhysicalDeviceSupportedExtensionNames(VkPhysicalDevice device) -> std::set<std::string>
    {
        auto extension_count = GetPhysicalDeviceSupportedExtensionNameCount(device);
        std::vector<VkExtensionProperties> extensions(extension_count);
        if (vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, extensions.data()) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not get the number of physical devices(graphics card) extensions.");
        }
        extensions.resize(extension_count);
        std::set<std::string> output;
        for (auto const & extension : extensions)
        {
            output.insert(extension.extensionName);
        }
        return output;
    }

    auto GetPhysicalDeviceSurfaceCapabilities(VkPhysicalDevice device, VkSurfaceKHR surface) -> VkSurfaceCapabilitiesKHR
    {
        VkSurfaceCapabilitiesKHR output = {};
        if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &output) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not get physical device's surface capibilities.");
        }
        return output;
    }

    auto GetPhysicalDeviceSurfaceFormats(VkPhysicalDevice device, VkSurfaceKHR surface) -> std::vector<VkSurfaceFormatKHR>
    {
        std::uint32_t format_count = 0;
        if (vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, nullptr) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not get physical device's surface format count.");
        }
        std::vector<VkSurfaceFormatKHR> output(format_count);
        if (vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, output.data()) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not get physical device's surface formats.");
        }
        return output;
    }

    auto GetPhysicalDeviceSurfacePresentModes(VkPhysicalDevice device, VkSurfaceKHR surface) -> std::vector<VkPresentModeKHR>
    {
        std::uint32_t present_modes_count = 0;
        if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &present_modes_count, nullptr) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not get physical device's surface's present mode count.");
        }
        std::vector<VkPresentModeKHR> output(present_modes_count);
        if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &present_modes_count, output.data()) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not get physical device's surface's present modes.");
        }
        return output;
    }

    SwapChainSupportDetails::SwapChainSupportDetails(VkPhysicalDevice device, VkSurfaceKHR surface)
        : capabilities(GetPhysicalDeviceSurfaceCapabilities(device, surface))
        , formats(GetPhysicalDeviceSurfaceFormats(device, surface))
        , present_modes(GetPhysicalDeviceSurfacePresentModes(device, surface))
    {}

    namespace
    {
        bool operator==(VkSurfaceFormatKHR const & lhs, VkSurfaceFormatKHR const & rhs)
        {
            return ( lhs.colorSpace == rhs.colorSpace and lhs.format == rhs.format );
        }
    }

    bool SwapChainSupportDetails::HasSurfaceFormat(VkSurfaceFormatKHR tested_format) const noexcept
    {
        for (auto const & format : formats)
        {
            if (format == tested_format)
            {
                return true;
            }
        }
        return false;
    }

    bool SwapChainSupportDetails::HasPresentMode(VkPresentModeKHR tested_present_mode) const noexcept
    {
        for (auto const & present_mode : present_modes)
        {
            if (present_mode == tested_present_mode)
            {
                return true;
            }
        }
        return false;
    }

	auto GetDeviceMemoryProperties(VkPhysicalDevice physical_device) -> VkPhysicalDeviceMemoryProperties
    {
        VkPhysicalDeviceMemoryProperties output{};
        vkGetPhysicalDeviceMemoryProperties(physical_device, &output);
        return output;
    }
}