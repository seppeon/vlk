#include "vlk/VkQueueFamilies.hpp"
#include <vulkan/vulkan.h>
#include <cstdint>
#include <vector>

namespace Vlk
{
	namespace
	{
		auto GetPhysicalDeviceQueueFamilyCount(VkPhysicalDevice physical_device) -> std::size_t
		{
			std::uint32_t output = 0;
			vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &output, nullptr);
			return output;
		}
	}

	auto GetPhysicalDeviceQueueFamilies(VkPhysicalDevice physical_device) -> std::vector<VkQueueFamilyProperties>
	{
		auto const family_count = GetPhysicalDeviceQueueFamilyCount(physical_device);
		std::uint32_t count = family_count;
		std::vector<VkQueueFamilyProperties> output(family_count);
		vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &count, output.data());
		output.resize(count);
		return output;
	}
}