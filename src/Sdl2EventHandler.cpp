#include "sdl2/Sdl2EventHandler.hpp"
#include "sdl2/Sdl2EventCommon.hpp"
#include "sdl2/Sdl2KeyEvent.hpp"
#include "sdl2/Sdl2MouseEvent.hpp"
#include <concepts>
#include <cstddef>
#include <variant>

namespace Sdl2
{
    namespace
    {
        namespace Impl
        {
            struct Handler
            {
                bool operator()(Sdl2::MouseEvent const & event) const noexcept
                {
                    return true;
                }
                bool operator()(Sdl2::KeyEvent const & event) const noexcept
                {
                    return true;
                }
                bool operator()(Sdl2::WindowEvent const & event) const noexcept
                {
                    return true;
                }
                bool operator()(Sdl2::AppEvent const & app) const noexcept
                {
                    return app != Sdl2::AppEvent::Quit;
                }
                bool operator()(auto const & event) const noexcept
                {
                    return true;
                }
            };

            template <typename T>
            struct Param
            {
                std::string name;
                T const & value;
            };
            
            template <typename ... Ts>
            std::string ToString(Param<Ts> const & ... args);
            template <typename ... Ts>
            std::string ToString(std::variant<Ts...> const & variant);
            template <typename T>
            std::string ToString(Param<T> const & param);

            std::string ToString(std::string const & value);

            std::string ToString(std::same_as<bool> auto value);

            std::string ToString(std::int32_t value);

            std::string ToString(float value);

            std::string ToString(std::uint32_t value);

            std::string ToString(std::uint64_t value);

            std::string ToString(Sdl2::EventVec2d const & value);

            std::string ToString(Sdl2::MouseEvent::Motion const & motion);

            std::string ToString(Sdl2::MouseButton mouse_button);

            std::string ToString(Sdl2::MouseButtonEvent const & button);

            std::string ToString(Sdl2::MouseEvent::Wheel const & motion);

            std::string ToString(Sdl2::MouseEvent const & mouse_event);

            std::string ToString(Sdl2::KeySides const & sides);

            std::string ToString(Sdl2::KeyStateModifiers const & state_modifiers);

            std::string ToString(Sdl2::KeyToggleModifiers const & toggle_modifiers);

            std::string ToString(Sdl2::KeyModifiers const & key_modifiers);

            std::string ToString(Sdl2::KeyCode const & code);

            std::string ToString(Sdl2::KeyEvent const & key_modifiers);
        
            std::string ToString(MouseButton mouse_button)
            {
                switch (mouse_button)
                {
                case MouseButton::Left: return "Left";
                case MouseButton::Middle: return "Middle";
                case MouseButton::Right: return "Right";
                case MouseButton::X1: return "X1";
                case MouseButton::X2: return "X2";
                default: return "";
                }
            }
            std::string ToString(DisplayEventType display_event_type)
            {
                switch(display_event_type)
                {
                case DisplayEventType::Orientation: return "Orientation";
                case DisplayEventType::Connected: return "Connected";
                case DisplayEventType::Disconnected: return "Disconnected";
                default: return "";
                }
            }

            std::string ToString(KeyCode const & code)
            {
                switch (code){
                case KeyCode::N0: return "N0";
                case KeyCode::N1: return "N1";
                case KeyCode::N2: return "N2";
                case KeyCode::N3: return "N3";
                case KeyCode::N4: return "N4";
                case KeyCode::N5: return "N5";
                case KeyCode::N6: return "N6";
                case KeyCode::N7: return "N7";
                case KeyCode::N8: return "N8";
                case KeyCode::N9: return "N9";
                case KeyCode::A: return "A";
                case KeyCode::B: return "B";
                case KeyCode::C: return "C";
                case KeyCode::D: return "D";
                case KeyCode::E: return "E";
                case KeyCode::F: return "F";
                case KeyCode::G: return "G";
                case KeyCode::H: return "H";
                case KeyCode::I: return "I";
                case KeyCode::J: return "J";
                case KeyCode::K: return "K";
                case KeyCode::L: return "L";
                case KeyCode::M: return "M";
                case KeyCode::N: return "N";
                case KeyCode::O: return "O";
                case KeyCode::P: return "P";
                case KeyCode::Q: return "Q";
                case KeyCode::R: return "R";
                case KeyCode::S: return "S";
                case KeyCode::T: return "T";
                case KeyCode::U: return "U";
                case KeyCode::V: return "V";
                case KeyCode::W: return "W";
                case KeyCode::X: return "X";
                case KeyCode::Y: return "Y";
                case KeyCode::Z: return "Z";
                case KeyCode::F1: return "F1";
                case KeyCode::F2: return "F2";
                case KeyCode::F3: return "F3";
                case KeyCode::F4: return "F4";
                case KeyCode::F5: return "F5";
                case KeyCode::F6: return "F6";
                case KeyCode::F7: return "F7";
                case KeyCode::F8: return "F8";
                case KeyCode::F9: return "F9";
                case KeyCode::F10: return "F10";
                case KeyCode::F11: return "F11";
                case KeyCode::F12: return "F12";
                case KeyCode::F13: return "F13";
                case KeyCode::F14: return "F14";
                case KeyCode::F15: return "F15";
                case KeyCode::F16: return "F16";
                case KeyCode::F17: return "F17";
                case KeyCode::F18: return "F18";
                case KeyCode::F19: return "F19";
                case KeyCode::F20: return "F20";
                case KeyCode::F21: return "F21";
                case KeyCode::F22: return "F22";
                case KeyCode::F23: return "F23";
                case KeyCode::F24: return "F24";
                case KeyCode::Down: return "Down";
                case KeyCode::Left: return "Left";
                case KeyCode::Right: return "Right";
                case KeyCode::Up: return "Up";
                case KeyCode::AC_Back: return "AC_Back";
                case KeyCode::AC_Bookmarks: return "AC_Bookmarks";
                case KeyCode::AC_Forward: return "AC_Forward";
                case KeyCode::AC_Home: return "AC_Home";
                case KeyCode::AC_Refresh: return "AC_Refresh";
                case KeyCode::AC_Search: return "AC_Search";
                case KeyCode::AC_Stop: return "AC_Stop";
                case KeyCode::Again: return "Again";
                case KeyCode::AltErase: return "AltErase";
                case KeyCode::App1: return "App1";
                case KeyCode::App2: return "App2";
                case KeyCode::Application: return "Application";
                case KeyCode::AudioFastForward: return "AudioFastForward";
                case KeyCode::AudioMute: return "AudioMute";
                case KeyCode::AudioNext: return "AudioNext";
                case KeyCode::AudioPlay: return "AudioPlay";
                case KeyCode::AudioPrevious: return "AudioPrevious";
                case KeyCode::AudioRewind: return "AudioRewind";
                case KeyCode::AudioStop: return "AudioStop";
                case KeyCode::Backslash: return "Backslash";
                case KeyCode::Backspace: return "Backspace";
                case KeyCode::BrightnessDown: return "BrightnessDown";
                case KeyCode::BrightnessUp: return "BrightnessUp";
                case KeyCode::Calculator: return "Calculator";
                case KeyCode::Call: return "Call";
                case KeyCode::Cancel: return "Cancel";
                case KeyCode::CapsLock: return "CapsLock";
                case KeyCode::Clear: return "Clear";
                case KeyCode::ClearAgain: return "ClearAgain";
                case KeyCode::Comma: return "Comma";
                case KeyCode::Computer: return "Computer";
                case KeyCode::Copy: return "Copy";
                case KeyCode::CrSel: return "CrSel";
                case KeyCode::CurrencySubunit: return "CurrencySubunit";
                case KeyCode::CurrencyUnit: return "CurrencyUnit";
                case KeyCode::Cut: return "Cut";
                case KeyCode::DecimalSeparator: return "DecimalSeparator";
                case KeyCode::Delete: return "Delete";
                case KeyCode::DisplaySwitch: return "DisplaySwitch";
                case KeyCode::Eject: return "Eject";
                case KeyCode::End: return "End";
                case KeyCode::EndCall: return "EndCall";
                case KeyCode::Equals: return "Equals";
                case KeyCode::Escape: return "Escape";
                case KeyCode::Execute: return "Execute";
                case KeyCode::ExSel: return "ExSel";
                case KeyCode::Find: return "Find";
                case KeyCode::Help: return "Help";
                case KeyCode::Home: return "Home";
                case KeyCode::Insert: return "Insert";
                case KeyCode::KeyboardIlluminationDown: return "KeyboardIlluminationDown";
                case KeyCode::KeyboardIlluminationUp: return "KeyboardIlluminationUp";
                case KeyCode::KeyboardIlluminationToggle: return "KeyboardIlluminationToggle";
                case KeyCode::KeyPad00: return "KeyPad00";
                case KeyCode::KeyPad000: return "KeyPad000";
                case KeyCode::KeyPad0: return "KeyPad0";
                case KeyCode::KeyPad1: return "KeyPad1";
                case KeyCode::KeyPad2: return "KeyPad2";
                case KeyCode::KeyPad3: return "KeyPad3";
                case KeyCode::KeyPad4: return "KeyPad4";
                case KeyCode::KeyPad5: return "KeyPad5";
                case KeyCode::KeyPad6: return "KeyPad6";
                case KeyCode::KeyPad7: return "KeyPad7";
                case KeyCode::KeyPad8: return "KeyPad8";
                case KeyCode::KeyPad9: return "KeyPad9";
                case KeyCode::KeyPadA: return "KeyPadA";
                case KeyCode::KeyPadB: return "KeyPadB";
                case KeyCode::KeyPadC: return "KeyPadC";
                case KeyCode::KeyPadD: return "KeyPadD";
                case KeyCode::KeyPadE: return "KeyPadE";
                case KeyCode::KeyPadF: return "KeyPadF";
                case KeyCode::KeyPadAmpersand: return "KeyPadAmpersand";
                case KeyCode::KeyPadAt: return "KeyPadAt";
                case KeyCode::KeyPadBackspace: return "KeyPadBackspace";
                case KeyCode::KeyPadBinary: return "KeyPadBinary";
                case KeyCode::KeyPadClear: return "KeyPadClear";
                case KeyCode::KeyPadClearEntry: return "KeyPadClearEntry";
                case KeyCode::KeyPadColon: return "KeyPadColon";
                case KeyCode::KeyPadComma: return "KeyPadComma";
                case KeyCode::KeyPadDoubleAmpersand: return "KeyPadDoubleAmpersand";
                case KeyCode::KeyPadDoubleVerticalBar: return "KeyPadDoubleVerticalBar";
                case KeyCode::KeyPadDecimal: return "KeyPadDecimal";
                case KeyCode::KeyPadDivide: return "KeyPadDivide";
                case KeyCode::KeyPadEnter: return "KeyPadEnter";
                case KeyCode::KeyPadEquals: return "KeyPadEquals";
                case KeyCode::KeyPadEqualsAS400: return "KeyPadEqualsAS400";
                case KeyCode::KeyPadExclamationMark: return "KeyPadExclamationMark";
                case KeyCode::KeyPadGreater: return "KeyPadGreater";
                case KeyCode::KeyPadHash: return "KeyPadHash";
                case KeyCode::KeyPadHexadecimal: return "KeyPadHexadecimal";
                case KeyCode::KeyPadLeftBrace: return "KeyPadLeftBrace";
                case KeyCode::KeyPadLeftParen: return "KeyPadLeftParen";
                case KeyCode::KeyPadLess: return "KeyPadLess";
                case KeyCode::KeyPadMemAdd: return "KeyPadMemAdd";
                case KeyCode::KeyPadMemClear: return "KeyPadMemClear";
                case KeyCode::KeyPadMemDivide: return "KeyPadMemDivide";
                case KeyCode::KeyPadMemMultiply: return "KeyPadMemMultiply";
                case KeyCode::KeyPadMemRecall: return "KeyPadMemRecall";
                case KeyCode::KeyPadMemStore: return "KeyPadMemStore";
                case KeyCode::KeyPadMemSubtract: return "KeyPadMemSubtract";
                case KeyCode::KeyPadMinus: return "KeyPadMinus";
                case KeyCode::KeyPadMultiply: return "KeyPadMultiply";
                case KeyCode::KeyPadOctal: return "KeyPadOctal";
                case KeyCode::KeyPadPercent: return "KeyPadPercent";
                case KeyCode::KeyPadPeriod: return "KeyPadPeriod";
                case KeyCode::KeyPadPlus: return "KeyPadPlus";
                case KeyCode::KeyPadPlusMinus: return "KeyPadPlusMinus";
                case KeyCode::KeyPadPower: return "KeyPadPower";
                case KeyCode::KeyPadRightBrace: return "KeyPadRightBrace";
                case KeyCode::KeyPadRightParen: return "KeyPadRightParen";
                case KeyCode::KeyPadSpace: return "KeyPadSpace";
                case KeyCode::KeyPadTab: return "KeyPadTab";
                case KeyCode::KeyPadVerticalBar: return "KeyPadVerticalBar";
                case KeyCode::KeyPadXor: return "KeyPadXor";
                case KeyCode::LeftAlt: return "LeftAlt";
                case KeyCode::LeftControl: return "LeftControl";
                case KeyCode::LeftBracket: return "LeftBracket";
                case KeyCode::LeftOs: return "LeftOs";
                case KeyCode::LeftShift: return "LeftShift";
                case KeyCode::Mail: return "Mail";
                case KeyCode::MediaSelect: return "MediaSelect";
                case KeyCode::Menu: return "Menu";
                case KeyCode::Minus: return "Minus";
                case KeyCode::Mode: return "Mode";
                case KeyCode::Mute: return "Mute";
                case KeyCode::NumLockClear: return "NumLockClear";
                case KeyCode::Operator: return "Operator";
                case KeyCode::Out: return "Out";
                case KeyCode::PageDown: return "PageDown";
                case KeyCode::PageUp: return "PageUp";
                case KeyCode::Paste: return "Paste";
                case KeyCode::Pause: return "Pause";
                case KeyCode::Period: return "Period";
                case KeyCode::Power: return "Power";
                case KeyCode::PrintScreen: return "PrintScreen";
                case KeyCode::Prior: return "Prior";
                case KeyCode::RightAlt: return "RightAlt";
                case KeyCode::RightControl: return "RightControl";
                case KeyCode::Return: return "Return";
                case KeyCode::Return2: return "Return2";
                case KeyCode::RightOs: return "RightOs";
                case KeyCode::RightBracket: return "RightBracket";
                case KeyCode::RightShift: return "RightShift";
                case KeyCode::ScrollLock: return "ScrollLock";
                case KeyCode::Select: return "Select";
                case KeyCode::Semicolon: return "Semicolon";
                case KeyCode::Seperator: return "Seperator";
                case KeyCode::Slash: return "Slash";
                case KeyCode::Sleep: return "Sleep";
                case KeyCode::SoftLeft: return "SoftLeft";
                case KeyCode::SoftRight: return "SoftRight";
                case KeyCode::Space: return "Space";
                case KeyCode::Stop: return "Stop";
                case KeyCode::SystemRequest: return "SystemRequest";
                case KeyCode::Tab: return "Tab";
                case KeyCode::ThousandsSeperator: return "ThousandsSeperator";
                case KeyCode::Undo: return "Undo";
                case KeyCode::Unknown: return "Unknown";
                case KeyCode::VolumeDown: return "VolumeDown";
                case KeyCode::VolumeUp: return "VolumeUp";
                case KeyCode::Web: return "Web";
                case KeyCode::Grave: return "Grave";
                case KeyCode::Apostrophe: return "Apostrophe";
                case KeyCode::NonUsHash: return "NonUsHash";
                case KeyCode::NonUsBackslash: return "NonUsBackslash";
                case KeyCode::Underscore: return "Underscore";
                case KeyCode::Asterisk: return "Asterisk";
                case KeyCode::DoubleQuote: return "DoubleQuote";
                case KeyCode::BackQuote: return "BackQuote";
                case KeyCode::Quote: return "Quote";
                case KeyCode::ExclamationMark: return "ExclamationMark";
                case KeyCode::Caret: return "Caret";
                case KeyCode::Question: return "Question";
                case KeyCode::Dollar: return "Dollar";
                default: return "Unknown";
                }
            }

            std::string ToString(std::string const & value)
            {
                return value;
            }

            std::string ToString(std::same_as<bool> auto value)
            {
                return value ? "true" : "false";
            }

            std::string ToString(std::int32_t value)
            {
                return std::to_string(value);
            }

            std::string ToString(std::int64_t value)
            {
                return std::to_string(value);
            }

            std::string ToString(float value)
            {
                return std::to_string(value);
            }

            std::string ToString(std::uint32_t value)
            {
                return std::to_string(value);
            }

            std::string ToString(std::uint64_t value)
            {
                return std::to_string(value);
            }

            std::string ToString(Sdl2::EventVec2d const & value)
            {
                return "{" + ToString(value.x) + ", " + ToString(value.y) + "}";
            }

            std::string ToString(Sdl2::MouseEvent::Motion const & motion)
            {
                return ToString( Param{ .name = "motion", .value = ToString(Param{ .name = "x", .value = motion.x }, Param{ .name = "y", .value = motion.y, }) });
            }

            std::string ToString(Sdl2::MouseButtonEvent const & button)
            {
                return ToString(
                    Param{ .name = "button", .value = button.button },
                    Param{
                        .name = "event",
                        .value = ToString(
                            Param{ .name = "id", .value = button.event.id },
                            Param{ .name = "count", .value = button.event.count },
                            Param{ .name = "state", .value = button.event.state }
                        )
                    }
                );
            }

            std::string ToString(Sdl2::MouseEvent::Wheel const & motion)
            {
                return ToString( Param{ .name = "wheel", .value = ToString(Param{ .name = "x", .value = motion.x }, Param{ .name = "y", .value = motion.y, })});
            }

            std::string ToString(Sdl2::MouseEvent const & mouse_event)
            {
                return ToString(
                    Param{
                        .name = "mouse_event",
                        .value = ToString(
                            Param{
                                .name = "mouse_instance_id",
                                .value = mouse_event.mouse_instance_id
                            },
                            Param{
                                .name = "coordinate",
                                .value = mouse_event.coordinate,
                            },
                            Param{
                                .name = "event_data",
                                .value = mouse_event.event_data
                            }
                        )
                    }
                );
            }

            std::string ToString(Sdl2::KeySides const & sides)
            {
                return ToString( Param{ .name = "left", .value = sides.left }, Param{ .name = "right", .value = sides.right, });
            }

            std::string ToString(Sdl2::KeyStateModifiers const & state_modifiers)
            {
                return ToString(
                    Param{ .name = "shift", .value = state_modifiers.shift },
                    Param{ .name = "ctrl", .value = state_modifiers.ctrl },
                    Param{ .name = "alt", .value = state_modifiers.alt },
                    Param{ .name = "os", .value = state_modifiers.os }
                );
            }

            std::string ToString(Sdl2::KeyToggleModifiers const & toggle_modifiers)
            {
                return ToString(
                    Param{ .name = "num_lock", .value = toggle_modifiers.num_lock },
                    Param{ .name = "caps_lock", .value = toggle_modifiers.caps_lock },
                    Param{ .name = "scroll_lock", .value = toggle_modifiers.scroll_lock }
                );
            }

            std::string ToString(Sdl2::KeyModifiers const & key_modifiers)
            {
                return ToString(
                    Param{ .name = "state", .value = key_modifiers.state },
                    Param{ .name = "toggle", .value = key_modifiers.toggle }
                );
            }

            std::string ToString(Sdl2::KeyEvent const & key_modifiers)
            {
                return ToString(
                    Param{ .name = "key_state", .value = key_modifiers.key_state },
                    Param{ .name = "is_repeat", .value = key_modifiers.is_repeat },
                    Param{ .name = "modifiers", .value = key_modifiers.modifiers },
                    Param{ .name = "key_code", .value = key_modifiers.key_code },
                    Param{ .name = "scan_code", .value = key_modifiers.scan_code }
                );
            }

            template <typename T>
            std::string ToString(Param<T> const & param)
            {
                return param.name + ":" + ToString(param.value);
            }

            template <typename ... Ts>
            std::string ToString(std::variant<Ts...> const & variant)
            {
                auto const to_string = [&](auto const & value)
                {
                    return ToString(value);
                };
                return std::visit(to_string, variant);
            }

            template <typename ... Ts>
            std::string ToString(Param<Ts> const & ... args)
            {
                auto output = "(" + ((ToString(args) + ", ") + ...);
                if (not output.empty())
                {
                    output.pop_back();
                    output.pop_back();
                }
                return output + ")";
            }

            struct WindowEventDescriber
            {
                [[nodiscard]] auto operator()(Sdl2::MouseEvent const & event) const noexcept -> std::string
                {
                    return ToString(event);
                }
                [[nodiscard]] auto operator()(Sdl2::KeyEvent const & event) const noexcept -> std::string
                {
                    return ToString(event);
                }
                [[nodiscard]] auto operator()(Sdl2::TextEvent const & e) const noexcept -> std::string
                {
                    return "TextEvent";
                }
                [[nodiscard]] auto operator()(Sdl2::TouchEvent const & e) const noexcept -> std::string
                {
                    return "TouchEvent";
                }
                [[nodiscard]] auto operator()(Sdl2::DropEvent const & e) const noexcept -> std::string
                {
                    return "DropEvent";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Shown const & e) const noexcept -> std::string
                {
                    return "Shown";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Hidden const & e) const noexcept -> std::string
                {
                    return "Hidden";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Exposed const & e) const noexcept -> std::string
                {
                    return "Exposed";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Moved const & e) const noexcept -> std::string
                {
                    return "Moved";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Resized const & e) const noexcept -> std::string
                {
                    return "Resized";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::SizeChanged const & e) const noexcept -> std::string
                {
                    return "SizeChanged";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Minimized const & e) const noexcept -> std::string
                {
                    return "Minimized";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Maximized const & e) const noexcept -> std::string
                {
                    return "Maximized";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Restored const & e) const noexcept -> std::string
                {
                    return "Restored";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Enter const & e) const noexcept -> std::string
                {
                    return "Enter";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Leave const & e) const noexcept -> std::string
                {
                    return "Leave";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::FocusGained const & e) const noexcept -> std::string
                {
                    return "FocusGained";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::FocusLost const & e) const noexcept -> std::string
                {
                    return "FocusLost";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::Close const & e) const noexcept -> std::string
                {
                    return "Close";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::TakeFocus const & e) const noexcept -> std::string
                {
                    return "TakeFocus";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::HitTest const & e) const noexcept -> std::string
                {
                    return "HitTest";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::IccprofChanged const & e) const noexcept -> std::string
                {
                    return "IccprofChanged";
                }
                [[nodiscard]] auto operator()(Sdl2::WindowEvent::DisplayChanged const & e) const noexcept -> std::string
                {
                    return "DisplayChanged";
                }
            };

            struct Describer
            {
                [[nodiscard]] auto operator()(Sdl2::WindowEvent const & event) const noexcept -> std::string
                {
                    return ToString(
                        Param{
                            .name = "window_id",
                            .value = event.window_id,
                        },
                        Param{
                            .name = "event_data",
                            .value = std::visit(WindowEventDescriber{}, event.event_data)
                        }
                    );
                }
                [[nodiscard]] auto operator()(Sdl2::AppEvent const & app) const noexcept -> std::string
                {
                    return "";
                }
                [[nodiscard]] auto operator()(auto const & event) const noexcept -> std::string
                {
                    return "";
                }
            };
        }
    }

    bool DefaultEventHandler(Hz::Span<Sdl2::Event const> events)
    {
        auto const handler = Impl::Handler{};
        for (auto const & event : events)
        {
            if (not std::visit(handler, event.event_data))
            {
                return false;
            }
        }
        return true;
    }

    std::string DescribeEvent(Sdl2::Event const & event)
    {
        return ToString(Impl::Param{.name = "timestamp", .value = event.timestamp.count()}, Impl::Param{ .name = "event", .value = std::visit(Impl::Describer{}, event.event_data)});
    }
}