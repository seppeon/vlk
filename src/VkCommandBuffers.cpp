#include "vlk/VkCommandBuffers.hpp"
#include <stdexcept>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	auto GetCommandPoolCreateInfo(std::uint32_t queue_family_index, bool commands_are_individually_rerecorded, bool commands_are_transient) -> VkCommandPoolCreateInfo
	{
		return {
			.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
			.pNext = nullptr,
			.flags = (
				std::uint32_t(commands_are_transient ? VK_COMMAND_POOL_CREATE_TRANSIENT_BIT : 0) |
				std::uint32_t(commands_are_individually_rerecorded ? VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT : 0)
			),
			.queueFamilyIndex = queue_family_index,
		};
	}

	auto CreateCommandPool(VkDevice device, VkCommandPoolCreateInfo const & create_info) -> CommandPool
	{
		CommandPool output(device, nullptr);
		if (vkCreateCommandPool(device, &create_info, nullptr, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("Cannot create command pool on the selected physical device.");
		}
		return output;
	}

	auto CreateCommandPool( VkDevice device, std::uint32_t queue_family_index, bool commands_are_individually_rerecorded, bool commands_are_transient ) -> CommandPool
	{
		return CreateCommandPool(device, GetCommandPoolCreateInfo(queue_family_index, commands_are_individually_rerecorded, commands_are_transient));
	}

	auto AllocateCommandBuffers(VkDevice device, VkCommandPool command_pool, bool is_primary, std::uint32_t command_buffer_count) -> CommandBuffers
	{
		auto const create_info = VkCommandBufferAllocateInfo{
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			.pNext = nullptr,
			.commandPool = command_pool,
			.level = (is_primary ? VK_COMMAND_BUFFER_LEVEL_PRIMARY : VK_COMMAND_BUFFER_LEVEL_SECONDARY),
			.commandBufferCount = command_buffer_count,
		};
		CommandBuffers output(device, command_pool, command_buffer_count);
		if (vkAllocateCommandBuffers(device, &create_info, output.data()) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate command buffers!");
		}
		return output;
	}

	bool BeginCommandBuffer(VkCommandBuffer command_buffer, CommandBufferFlags settings, VkCommandBufferInheritanceInfo const * inheritance_info)
	{
		auto const begin_info = VkCommandBufferBeginInfo{
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			.pNext = nullptr,
			.flags = (
				std::uint32_t(settings.one_time_submit ? VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT : 0) |
				std::uint32_t(settings.render_pass_continue ? VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT : 0) |
				std::uint32_t(settings.simultaneous_use ? VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT : 0)
			),
			.pInheritanceInfo = inheritance_info,
		};
		return vkBeginCommandBuffer(command_buffer, &begin_info) == VK_SUCCESS;
	}

	namespace
	{
		void BeginInlineRenderPassImpl( VkCommandBuffer command_buffer, VkRenderPass render_pass, VkFramebuffer frame_buffer, VkRect2D render_area, Hz::Span<const VkClearValue> clear_values, VkSubpassContents subpass_contents)
		{
			VkRenderPassBeginInfo begin_info{
				.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
				.pNext = nullptr,
				.renderPass = render_pass,
				.framebuffer = frame_buffer,
				.renderArea = render_area,
				.clearValueCount = static_cast<std::uint32_t>(clear_values.Size()),
				.pClearValues = clear_values.Data(),
			};
			vkCmdBeginRenderPass(command_buffer, &begin_info, subpass_contents);
		}
	}

	void CommandBeginRenderPassInline( VkCommandBuffer command_buffer, VkRenderPass render_pass, VkFramebuffer frame_buffer, VkRect2D render_area, Hz::Span<const VkClearValue> clear_values)
	{
		BeginInlineRenderPassImpl(command_buffer, render_pass, frame_buffer, render_area, clear_values, VK_SUBPASS_CONTENTS_INLINE);
	}

	void CommandBeginRenderPassSecondaryCommandBuffers( VkCommandBuffer command_buffer, VkRenderPass render_pass, VkFramebuffer frame_buffer, VkRect2D render_area, Hz::Span<const VkClearValue> clear_values)
	{
		BeginInlineRenderPassImpl(command_buffer, render_pass, frame_buffer, render_area, clear_values, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
	}

	void CommandSetScissor(VkCommandBuffer command_buffer, Hz::Span<VkRect2D const> scissors, std::size_t first_scissor)
	{
		vkCmdSetScissor(command_buffer, static_cast<std::uint32_t>(first_scissor), static_cast<std::uint32_t>(scissors.Size()), scissors.Data());
	}

	void CommandSetViewport(VkCommandBuffer command_buffer, Hz::Span<VkViewport const> viewports, std::size_t first_viewport)
	{
		vkCmdSetViewport(command_buffer, static_cast<std::uint32_t>(first_viewport), static_cast<std::uint32_t>(viewports.Size()), viewports.Data());
	}

	void CommandBindGraphicsPipeline(VkCommandBuffer command_buffer, VkPipeline pipeline)
	{
		vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
	}

	void CommandBindComputePipeline(VkCommandBuffer command_buffer, VkPipeline pipeline)
	{
		vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
	}

	void CommandDraw(VkCommandBuffer command_buffer, std::size_t vertex_count, std::size_t instance_count, std::size_t first_vertex, std::size_t first_instance)
	{
		vkCmdDraw(command_buffer,
			static_cast<std::uint32_t>(vertex_count),
			static_cast<std::uint32_t>(instance_count),
			static_cast<std::uint32_t>(first_vertex),
			static_cast<std::uint32_t>(first_instance)
		);
	}

	void CommandDrawIndexed(VkCommandBuffer command_buffer, std::size_t vertex_count, std::size_t instance_count, std::size_t first_index, std::size_t vertex_offset, std::size_t first_instance)
	{
		vkCmdDrawIndexed(command_buffer,
			static_cast<std::uint32_t>(vertex_count),
			static_cast<std::uint32_t>(instance_count),
			static_cast<std::uint32_t>(first_index),
			static_cast<std::uint32_t>(vertex_offset),
			static_cast<std::uint32_t>(first_instance)
		);
	}

	void CommandEndRenderPass(VkCommandBuffer command_buffer)
	{
		vkCmdEndRenderPass(command_buffer);
	}

	void EndCommandBuffer(VkCommandBuffer command_buffer)
	{
		if (vkEndCommandBuffer(command_buffer) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to record command buffer.");
		}
	}

	void ResetCommandBuffer(VkCommandBuffer command_buffer)
	{
		vkResetCommandBuffer(command_buffer, 0);
	}

	void CommandBindU32IndexBuffers(VkCommandBuffer command_buffer, VkBuffer buffer, std::size_t offset)
	{
		vkCmdBindIndexBuffer(command_buffer, buffer, offset, VK_INDEX_TYPE_UINT32);
	}

	void CommandBindVertexBuffers(VkCommandBuffer command_buffer, uint32_t first_binding, Hz::Span<VkBuffer const> buffers, Hz::Span<VkDeviceSize const> offsets)
	{
		vkCmdBindVertexBuffers(command_buffer, first_binding, static_cast<uint32_t>(buffers.Size()), buffers.Data(), offsets.Data());
	}

	void CommandBindVertexBuffer(VkCommandBuffer command_buffer, uint32_t first_binding, Buffer const & buffer, std::size_t offset)
	{
		CommandBindVertexBuffers(command_buffer, first_binding, {1, &buffer}, {1, &offset});
	}

	void CommandCopyBuffer(VkCommandBuffer command_buffer, VkBuffer src, VkBuffer dst, Hz::Span<VkBufferCopy const> regions)
	{
		vkCmdCopyBuffer(command_buffer, src, dst, static_cast<std::uint32_t>(regions.Size()), regions.Data());
	}

	void CommandBindDescriptorSets(VkCommandBuffer commandBuffer, VkPipelineBindPoint pipeline_bind_point, VkPipelineLayout layout, uint32_t first_set, Hz::Span<const VkDescriptorSet> descriptor_sets, Hz::Span<const uint32_t> dynamic_offset)
	{
		vkCmdBindDescriptorSets(commandBuffer, pipeline_bind_point, layout, first_set, uint32_t(descriptor_sets.Size()), descriptor_sets.Data(), uint32_t(dynamic_offset.Size()), dynamic_offset.Data());
	}
	// vkCmdBindDescriptorBufferEmbeddedSamplersEXT
	// vkCmdBindDescriptorBuffersEXT
	// vkCmdBindDescriptorSets
	// vkCmdBindIndexBuffer
	// vkCmdBindPipeline
	// vkCmdBindTransformFeedbackBuffersEXT
	// vkCmdBindVertexBuffers
	// vkCmdBindVertexBuffers2
	// vkCmdBindVertexBuffers2EXT
	// vkCmdBlitImage
	// vkCmdBlitImage2
	// vkCmdBlitImage2KHR
	// vkCmdBuildAccelerationStructuresIndirectKHR
	// vkCmdBuildAccelerationStructuresKHR
	// vkCmdBuildMicromapsEXT
	// vkCmdClearAttachments
	// vkCmdClearColorImage
	// vkCmdClearDepthStencilImage
	// vkCmdControlVideoCodingKHR
	// vkCmdCopyAccelerationStructureKHR
	// vkCmdCopyAccelerationStructureToMemoryKHR
	// vkCmdCopyBuffer
	// vkCmdCopyBuffer2
	// vkCmdCopyBuffer2KHR
	// vkCmdCopyBufferToImage
	// vkCmdCopyBufferToImage2
	// vkCmdCopyBufferToImage2KHR
	// vkCmdCopyImage
	// vkCmdCopyImage2
	// vkCmdCopyImage2KHR
	// vkCmdCopyImageToBuffer
	// vkCmdCopyImageToBuffer2
	// vkCmdCopyImageToBuffer2KHR
	// vkCmdCopyMemoryToAccelerationStructureKHR
	// vkCmdCopyMemoryToMicromapEXT
	// vkCmdCopyMicromapEXT
	// vkCmdCopyMicromapToMemoryEXT
	// vkCmdCopyQueryPoolResults
	// vkCmdCuLaunchKernelNVX
	// vkCmdDebugMarkerBeginEXT
	// vkCmdDebugMarkerEndEXT
	// vkCmdDebugMarkerInsertEXT
	// vkCmdDecodeVideoKHR
	// vkCmdDispatch
	// vkCmdDispatchBase
	// vkCmdDispatchBaseKHR
	// vkCmdDispatchIndirect
	// vkCmdDraw
	// vkCmdDrawIndexed
	// vkCmdDrawIndexedIndirect
	// vkCmdDrawIndexedIndirectCount
	// vkCmdDrawIndexedIndirectCountKHR
	// vkCmdDrawIndirect
	// vkCmdDrawIndirectByteCountEXT
	// vkCmdDrawIndirectCount
	// vkCmdDrawIndirectCountKHR
	// vkCmdDrawMeshTasksEXT
	// vkCmdDrawMeshTasksIndirectCountEXT
	// vkCmdDrawMeshTasksIndirectEXT
	// vkCmdDrawMultiEXT
	// vkCmdDrawMultiIndexedEXT
	// vkCmdExecuteCommands
	// vkCmdFillBuffer
	// vkCmdInsertDebugUtilsLabelEXT
	// vkCmdNextSubpass
	// vkCmdNextSubpass2
	// vkCmdNextSubpass2KHR
	// vkCmdPipelineBarrier
	// vkCmdPipelineBarrier2
	// vkCmdPipelineBarrier2KHR
	// vkCmdPushConstant
	// vkCmdPushDescriptorSetKHR
	// vkCmdPushDescriptorSetWithTemplateKHR
	// vkCmdResetEven
	// vkCmdResetEvent2
	// vkCmdResetEvent2KHR
	// vkCmdResetQueryPool
	// vkCmdResolveImage
	// vkCmdResolveImage2
	// vkCmdResolveImage2KHR
	// vkCmdSetAlphaToCoverageEnableEXT
	// vkCmdSetAlphaToOneEnableEXT
	// vkCmdSetBlendConstants
	// vkCmdSetColorBlendAdvancedEXT
	// vkCmdSetColorBlendEnableEXT
	// vkCmdSetColorBlendEquationEXT
	// vkCmdSetColorWriteMaskEXT
	// vkCmdSetConservativeRasterizationModeEXT
	// vkCmdSetCullMode
	// vkCmdSetCullModeEXT
	// vkCmdSetDepthBias
	// vkCmdSetDepthBiasEnable
	// vkCmdSetDepthBiasEnableEXT
	// vkCmdSetDepthBounds
	// vkCmdSetDepthBoundsTestEnable
	// vkCmdSetDepthBoundsTestEnableEXT
	// vkCmdSetDepthClampEnableEXT
	// vkCmdSetDepthClipEnableEXT
	// vkCmdSetDepthClipNegativeOneToOneEXT
	// vkCmdSetDepthCompareOp
	// vkCmdSetDepthCompareOpEXT
	// vkCmdSetDepthTestEnable
	// vkCmdSetDepthTestEnableEXT
	// vkCmdSetDepthWriteEnable
	// vkCmdSetDepthWriteEnableEXT
	// vkCmdSetDescriptorBufferOffsetsEXT
	// vkCmdSetDeviceMask
	// vkCmdSetDeviceMaskKHR
	// vkCmdSetDiscardRectangleEnableEXT
	// vkCmdSetDiscardRectangleEXT
	// vkCmdSetDiscardRectangleModeEXT
	// vkCmdSetEvent
	// vkCmdSetEvent2
	// vkCmdSetEvent2KHR
	// vkCmdSetExtraPrimitiveOverestimationSizeEXT
	// vkCmdSetFragmentShadingRateKHR
	// vkCmdSetFrontFace
	// vkCmdSetFrontFaceEXT
	// vkCmdSetLineRasterizationModeEXT
	// vkCmdSetLineStippleEnableEXT
	// vkCmdSetLineStippleEXT
	// vkCmdSetLineWidth
	// vkCmdSetLogicOpEnableEXT
	// vkCmdSetLogicOpEXT
	// vkCmdSetPatchControlPointsEXT
	// vkCmdSetPolygonModeEXT
	// vkCmdSetPrimitiveRestartEnable
	// vkCmdSetPrimitiveRestartEnableEXT
	// vkCmdSetPrimitiveTopology
	// vkCmdSetPrimitiveTopologyEXT
	// vkCmdSetProvokingVertexModeEXT
	// vkCmdSetRasterizationSamplesEXT
	// vkCmdSetRasterizationStreamEXT
	// vkCmdSetRasterizerDiscardEnable
	// vkCmdSetRasterizerDiscardEnableEXT
	// vkCmdSetRayTracingPipelineStackSizeKHR
	// vkCmdSetSampleLocationsEnableEXT
	// vkCmdSetSampleLocationsEXT
	// vkCmdSetSampleMaskEXT
	// vkCmdSetScissor
	// vkCmdSetScissorWithCount
	// vkCmdSetScissorWithCountEXT
	// vkCmdSetStencilCompareMask
	// vkCmdSetStencilOp
	// vkCmdSetStencilOpEXT
	// vkCmdSetStencilReference
	// vkCmdSetStencilTestEnable
	// vkCmdSetStencilTestEnableEXT
	// vkCmdSetStencilWriteMask
	// vkCmdSetTessellationDomainOriginEXT
	// vkCmdSetVertexInputEXT
	// vkCmdSetViewport
	// vkCmdSetViewportWithCount
	// vkCmdSetViewportWithCountEXT
	// vkCmdTraceRaysIndirect2KHR
	// vkCmdTraceRaysIndirectKHR
	// vkCmdTraceRaysKHR
	// vkCmdUpdateBuffer
	// vkCmdWaitEvents
	// vkCmdWaitEvents2
	// vkCmdWaitEvents2KHR
	// vkCmdWriteAccelerationStructuresPropertiesKHR
	// vkCmdWriteMicromapsPropertiesEXT
	// vkCmdWriteTimestamp
	// vkCmdWriteTimestamp2
	// vkCmdWriteTimestamp2KHR
}