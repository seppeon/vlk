#include "vlk/VkSync.hpp"
#include <cstdint>
#include <stdexcept>
#include <utility>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	auto GetSemaphoreCreateInfo() -> VkSemaphoreCreateInfo
	{
		return {
			.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
			.pNext = nullptr,
			.flags = VkSemaphoreCreateFlags{}
		};
	}

	auto CreateSemaphore(VkDevice device, VkSemaphoreCreateInfo const & create_info, VkAllocationCallbacks const * allocators) -> Semaphore
	{
		Semaphore output(device, allocators);
		if (vkCreateSemaphore(device, &create_info, allocators, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("Unable to create semaphore.");
		}
		return output;
	}

	auto GetFenceCreateInfo(bool signaled) -> VkFenceCreateInfo
	{
		return {
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
			.pNext = nullptr,
			.flags = signaled ? VK_FENCE_CREATE_SIGNALED_BIT : VkFenceCreateFlags{},
		};
	}

	auto CreateFence(VkDevice device, VkFenceCreateInfo const & create_info, VkAllocationCallbacks const * allocators) -> Fence
	{
		Fence output(device, allocators);
		if (vkCreateFence(device, &create_info, allocators, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("Unable to create fence.");
		}
		return output;
	}
		
	auto CreateFence(VkDevice device, bool signaled, VkAllocationCallbacks const * allocators) -> Fence
	{
		return CreateFence(device, GetFenceCreateInfo(signaled), allocators);
	}

	bool WaitForAllFences(VkDevice device, Hz::Span<VkFence const> fences, std::chrono::nanoseconds timeout)
	{
		auto const len = static_cast<std::uint32_t>(fences.Size());
		if (vkWaitForFences(device, len, fences.Data(), VK_TRUE, timeout.count()) != VK_SUCCESS)
		{
			return false;
		}
		if (vkResetFences(device, len, fences.Data()) != VK_SUCCESS)
		{
			return false;
		}
		return true;
	}

	bool WaitForAnyFences(VkDevice device, Hz::Span<VkFence const> fences, std::chrono::nanoseconds timeout)
	{
		auto const len = static_cast<std::uint32_t>(fences.Size());
		return (
			(vkWaitForFences(device, len, fences.Data(), VK_FALSE, timeout.count()) == VK_SUCCESS) and
			(vkResetFences(device, len, fences.Data()) == VK_SUCCESS)
		);
	}

	auto AcquireNextImage(VkDevice device, VkSwapchainKHR swap_chain, VkSemaphore signal_semaphore, VkFence signal_fence, std::chrono::nanoseconds timeout) -> std::uint32_t
	{
		std::uint32_t output = 0;
		return
			(::vkAcquireNextImageKHR(device, swap_chain, timeout.count(), signal_semaphore, signal_fence, &output) == VK_SUCCESS)
			? output
			: ~std::uint32_t{};
	}

	auto AcquireNextImage(VkDevice device, VkSwapchainKHR swap_chain, VkSemaphore signal_semaphore, std::chrono::nanoseconds timeout) -> std::uint32_t
	{
		return Vlk::AcquireNextImage(device, swap_chain, signal_semaphore, nullptr, timeout); 
	}

	auto AcquireNextImage(VkDevice device, VkSwapchainKHR swap_chain, VkFence signal_fence, std::chrono::nanoseconds timeout) -> std::uint32_t
	{
		return Vlk::AcquireNextImage(device, swap_chain, nullptr, signal_fence, timeout); 
	}

	auto GetSubmitCreateInfo( Hz::Span<VkSemaphore const> wait_semaphores, Hz::Span<VkPipelineStageFlags const> wait_dst_stage_mask, CommandBuffers const & command_buffers, Hz::Span<VkSemaphore const> signal_semaphores ) -> VkSubmitInfo
	{
		return VkSubmitInfo{
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.pNext = nullptr,
			.waitSemaphoreCount = static_cast<std::uint32_t>( wait_semaphores.Size() ),
			.pWaitSemaphores = wait_semaphores.Data(),
			.pWaitDstStageMask = wait_dst_stage_mask.Data(),
			.commandBufferCount = static_cast<std::uint32_t>( command_buffers.size() ),
			.pCommandBuffers = command_buffers.data(),
			.signalSemaphoreCount = static_cast<std::uint32_t>( signal_semaphores.Size() ),
			.pSignalSemaphores = signal_semaphores.Data(),
		};
	}
}