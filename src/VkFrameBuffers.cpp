#include "vlk/VkFramebuffers.hpp"
#include <cstddef>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	auto GetFrameBufferCreateInfo(VkRenderPass render_pass, Hz::Span<VkImageView const> attachments, VkExtent2D extents, std::uint32_t layout_count) -> VkFramebufferCreateInfo
	{
		return VkFramebufferCreateInfo
		{
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.renderPass = render_pass,
			.attachmentCount = static_cast<std::uint32_t>(attachments.Size()),
			.pAttachments = attachments.Data(),
			.width = extents.width,
			.height = extents.height,
			.layers = layout_count,
		};
	}

	auto CreateFrameBuffer(VkDevice device, VkFramebufferCreateInfo const & create_info) -> FrameBuffer
	{
		FrameBuffer output(device, nullptr);
		if (vkCreateFramebuffer(device, &create_info, nullptr, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("cannot create frame buffer.");
		}
		return output;
	}

	[[nodiscard]] auto CreateFrameBuffers(VkDevice device, Hz::Span<VkFramebufferCreateInfo const> create_infos) -> std::vector<FrameBuffer>
	{
		std::vector<FrameBuffer> output;
		output.reserve(create_infos.Size());
		for (std::size_t i = 0; i < create_infos.Size(); ++i)
		{
			output.emplace_back(Vlk::CreateFrameBuffer(device, create_infos[i]));
		}
		return output;
	}

	auto CreateFrameBuffers(VkDevice device, VkRenderPass render_pass, VkExtent2D const & swap_extents, Hz::Span<ImageView const> image_views) -> std::vector<FrameBuffer>
	{
		std::vector<FrameBuffer> output;
		output.reserve(image_views.Size());
		for (auto const & image_view : image_views)
		{
			auto const create_infos = Vlk::GetFrameBufferCreateInfo( render_pass, { 1, &image_view }, swap_extents, 1 );
			output.emplace_back(Vlk::CreateFrameBuffer(device, create_infos));
		}
		return output;
	}
}