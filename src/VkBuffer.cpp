#include "vlk/VkBuffer.hpp"
#include <stdexcept>

namespace Vlk
{
	auto GetBufferCreateInfo(std::size_t buffer_size, Hz::Span<std::uint32_t const> queue_family_indicies, VkBufferUsageFlags usage_flag) -> VkBufferCreateInfo
	{
		return VkBufferCreateInfo{
			.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.size = static_cast<std::uint32_t>(buffer_size),
			.usage = usage_flag,
			.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
			.queueFamilyIndexCount = static_cast<std::uint32_t>(queue_family_indicies.Size()),
			.pQueueFamilyIndices = queue_family_indicies.Data(),
		};
	}

	auto GetExclusiveBufferCreateInfo(std::size_t buffer_size) -> VkBufferCreateInfo
	{
		return GetBufferCreateInfo(buffer_size, {}, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
	}

	auto GetConcurrentBufferCreateInfo(std::size_t buffer_size, Hz::Span<std::uint32_t const> queue_family_indicies) -> VkBufferCreateInfo
	{
		return GetBufferCreateInfo(buffer_size, queue_family_indicies, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
	}

	auto CreateBuffer(VkDevice device, VkBufferCreateInfo const & create_info, VkAllocationCallbacks const * allocator) -> Buffer
	{
		Buffer output{device, allocator};
		if (vkCreateBuffer(device, &create_info, allocator, &output) != VK_SUCCESS)
		{
        	throw std::runtime_error("failed to create vertex buffer!");
		}
		return output;
	}

	auto CreateBuffer( VkDevice device, std::size_t buffer_size, Hz::Span<std::uint32_t const> queue_family_indicies, VkBufferUsageFlags usage_flag, VkAllocationCallbacks const * allocator ) -> Buffer
	{
		return CreateBuffer(device, GetBufferCreateInfo(buffer_size, queue_family_indicies, usage_flag), allocator);
	}

	auto GetBufferMemoryRequirements(VkDevice device, VkBuffer buffer) -> VkMemoryRequirements
	{
		VkMemoryRequirements output{};
		vkGetBufferMemoryRequirements(device, buffer, &output);
		return output;
	}

	std::size_t GetSupportedMemory(std::uint32_t type_mask, VkPhysicalDeviceMemoryProperties const & supported_mem_properties, VkMemoryPropertyFlags mem_properties)
	{
		auto const remaining_options = [&]
		{
			return type_mask != 0;
		};
		auto const pop_memory_type_index = [&]
		{
			auto const lowest_one_bit = type_mask & (-type_mask);
			type_mask -= lowest_one_bit;
			return std::size_t(std::countr_zero(lowest_one_bit));
		};
		auto const meets_requirements = [=](VkMemoryPropertyFlags value) -> bool
		{
			return (value & mem_properties) == mem_properties;
		};
		while ( remaining_options() )
		{
			auto const test_index = pop_memory_type_index();
			auto const & [property_flags, heap_index] = supported_mem_properties.memoryTypes[test_index];
			if ( meets_requirements(property_flags) ) { return test_index; }
		}
		return no_supported_memory;
	}
}