#include "vlk/VkToString.hpp"

namespace Vlk
{
	auto ToString(VkMemoryType const & memory_type) -> std::string
	{
		std::string output;
		output += "{ \"heapIndex\": ";
		output += std::to_string(memory_type.heapIndex) + ", \"propertyFlags\": \"";
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
		{
			output += "DEVICE_LOCAL_BIT | ";
		}
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
		{
			output += "HOST_VISIBLE_BIT | ";
		}
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
		{
			output += "HOST_COHERENT_BIT | ";
		}
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
		{
			output += "HOST_CACHED_BIT | ";
		}
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)
		{
			output += "LAZILY_ALLOCATED_BIT | ";
		}
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_PROTECTED_BIT)
		{
			output += "PROTECTED_BIT | ";
		}
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD)
		{
			output += "DEVICE_COHERENT_BIT_AMD | ";
		}
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD)
		{
			output += "DEVICE_UNCACHED_BIT_AMD | ";
		}
    	if (memory_type.propertyFlags & VK_MEMORY_PROPERTY_RDMA_CAPABLE_BIT_NV)
		{
			output += "RDMA_CAPABLE_BIT_NV | ";
		}
		if (memory_type.propertyFlags)
		{
			output.pop_back();
			output.pop_back();
			output.pop_back();
		}
		return output + "\" }";
	}
}