#include "vlk/VkCreateInstance.hpp"
#include "vlk/VkUtil.hpp"
#include <stdexcept>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
    auto AppInfo(Versioned const & app, Versioned const & engine, FL::Semver api_version) -> VkApplicationInfo
    {
        return VkApplicationInfo{
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pNext = nullptr,
            .pApplicationName = app.name.data(),
            .applicationVersion = ToVkVersion(app.version),
            .pEngineName = engine.name.data(),
            .engineVersion = ToVkVersion(engine.version),
            .apiVersion = ToVkVersion(api_version),
        };
    }

    auto CreateInstance(VkApplicationInfo const & app_info, std::set<std::string> const & required_layers, std::set<std::string> const & required_extensions) -> Instance
    {
        auto c_required_layers = ToCStrVector(required_layers);
        auto c_required_extensions = ToCStrVector(required_extensions);
        auto create_info = VkInstanceCreateInfo{
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pNext = nullptr,
            .pApplicationInfo = &app_info,
            .enabledLayerCount = static_cast<std::uint32_t>(c_required_layers.size()),
            .ppEnabledLayerNames = c_required_layers.data(),
            .enabledExtensionCount = static_cast<std::uint32_t>(c_required_extensions.size()),
            .ppEnabledExtensionNames = c_required_extensions.data(),
        };
#ifdef __APPLE__
        std::vector<const char*> requiredExtensions{ create_info.ppEnabledExtensionNames, create_info.ppEnabledExtensionNames + create_info.enabledExtensionCount };
        requiredExtensions.emplace_back(VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME);
        auto create_info = create_info;
        create_info.flags |= VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR;
        create_info.ppEnabledExtensionNames = requiredExtensions.data();
        create_info.enabledExtensionCount = static_cast<std::uint32_t>(requiredExtensions.size());
#endif
        Instance output( nullptr );
        if (vkCreateInstance(&create_info, nullptr, &output) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not create vulkan instance.");
        }
        return output;
    }
}