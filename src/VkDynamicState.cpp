#include "vlk/VkDynamicState.hpp"
#include <cstdint>

namespace Vlk
{
	auto GetDynamicStateCreateInfo(Hz::Span<VkDynamicState const> dynamic_states) -> VkPipelineDynamicStateCreateInfo
	{
		return VkPipelineDynamicStateCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
			.pNext = nullptr,
			.dynamicStateCount = static_cast<std::uint32_t>(dynamic_states.Size()),
			.pDynamicStates = dynamic_states.Data(),
		};
	}
}