#include "vlk/VkValidationLayers.hpp"
#include <stdexcept>
#include <vector>
#include <cstdint>
#include <vulkan/vulkan_core.h>


extern "C" VKAPI_ATTR void VKAPI_CALL vkDestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT messenger, const VkAllocationCallbacks* pAllocator)
{
	auto const fn = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	fn(instance, messenger, pAllocator);
}

namespace Vlk
{
    auto GetLayersCount() -> std::size_t
	{
		uint32_t output = 0;
		if (vkEnumerateInstanceLayerProperties(&output, nullptr) != VK_SUCCESS)
		{
			throw std::runtime_error("The number of supported layers could not be determined.");
		}
		return output;
	}

    auto GetSupportedLayers(std::size_t layer_count) -> std::set<std::string>
	{
    	std::vector<VkLayerProperties> avaliable_layers(layer_count);
		auto count = static_cast<std::uint32_t>(layer_count);
    	if (vkEnumerateInstanceLayerProperties(&count, avaliable_layers.data()) != VK_SUCCESS)
		{
			throw std::runtime_error("The supported layers could not be read.");
		}
		avaliable_layers.resize(count);
		std::set<std::string> output;
		for (auto const & layer : avaliable_layers) { output.insert(layer.layerName); }
		return output;
	}

	namespace
	{
		VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerCreateInfoEXT const * create_info, VkAllocationCallbacks const * allocator, VkDebugUtilsMessengerEXT * debug_messenger)
		{
			auto const func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
			return (func != nullptr)
				? func(instance, create_info, allocator, debug_messenger)
				: VK_ERROR_EXTENSION_NOT_PRESENT;
		}

		VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT severity, VkDebugUtilsMessageTypeFlagsEXT type, VkDebugUtilsMessengerCallbackDataEXT const * data, void* ptr)
		{
			auto const & callback = *static_cast<DebugCallback const *>(ptr);
			callback(data->pMessage);
			return VK_FALSE;
		}
	}

	auto Impl::GetDebugUtilsMessengerCreateInfo(DebugUtilsSettings settings, DebugCallback const & callback) -> VkDebugUtilsMessengerCreateInfoEXT
	{
		return VkDebugUtilsMessengerCreateInfoEXT{
			.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
			.pNext = {},
			.flags = {},
			.messageSeverity = (
				( settings.enable_category_verbose ? VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT : VkDebugUtilsMessageSeverityFlagsEXT{}) |
				( settings.enable_category_info ? VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT : VkDebugUtilsMessageSeverityFlagsEXT{}) |
				( settings.enable_category_warning ? VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT : VkDebugUtilsMessageSeverityFlagsEXT{}) |
				( settings.enable_category_error ? VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT : VkDebugUtilsMessageSeverityFlagsEXT{})
			),
			.messageType =  (
				( settings.enable_type_general ? VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT : VkDebugUtilsMessageTypeFlagsEXT{}) |
				( settings.enable_type_validation ? VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT : VkDebugUtilsMessageTypeFlagsEXT{}) |
				( settings.enable_type_performance ? VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT : VkDebugUtilsMessageTypeFlagsEXT{}) |
				( settings.enable_type_device_address_binding ? VK_DEBUG_UTILS_MESSAGE_TYPE_DEVICE_ADDRESS_BINDING_BIT_EXT : VkDebugUtilsMessageTypeFlagsEXT{})
			),
			.pfnUserCallback = &debug_callback,
			.pUserData = const_cast<void*>(static_cast<void const *>(&callback)),
		};
	}


	auto CreateDebugUtilsMessenger(VkInstance instance, VkDebugUtilsMessengerCreateInfoEXT const & create_info, VkAllocationCallbacks const * allocators) -> DebugUtilsMessengerEXT
	{
		DebugUtilsMessengerEXT output(instance, allocators);
		if (CreateDebugUtilsMessengerEXT(instance, &create_info, allocators, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("Cannot create debug utils messenger.");
		}
		return output;
	}
}