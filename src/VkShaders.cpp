#include "Vlk/VkShaders.hpp"
#include <FL/File.hpp>
#include <filesystem>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	auto CreateShaderFromCode(VkDevice device, Hz::Span<char const> code) -> ShaderModule
	{
		auto const create_info = VkShaderModuleCreateInfo{
			.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.codeSize = code.Size(),
			.pCode = reinterpret_cast<const uint32_t*>(code.Data()),
		};
		ShaderModule output(device, nullptr);
		if (vkCreateShaderModule(device, &create_info, nullptr, &output) != VK_SUCCESS) {
			throw std::runtime_error("could not create shader module.");
		}
		return output;
	}

	auto CreateShaderModuleFromFile(VkDevice device, std::filesystem::path file_path) -> ShaderModule
	{
		if (not (std::filesystem::exists(file_path) and std::filesystem::is_regular_file(file_path)))
		{
			throw std::runtime_error("shader path is invalid '" + file_path.string() + "'.");
		}
		FL::File file(file_path);
		return CreateShaderFromCode(device, file.Read());
	}

	auto GetPipelineStageVertexShaderCreateInfo(VkShaderModule shader_module, std::string_view name) -> VkPipelineShaderStageCreateInfo
	{
		return VkPipelineShaderStageCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.stage = VK_SHADER_STAGE_VERTEX_BIT,
			.module = shader_module,
			.pName = name.data(),
			.pSpecializationInfo = nullptr,
		};
	}

	auto GetPipelineStageFragmentShaderCreateInfo(VkShaderModule shader_module, std::string_view name) -> VkPipelineShaderStageCreateInfo
	{
		return VkPipelineShaderStageCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.pNext = nullptr,
			.flags = {},
			.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
			.module = shader_module,
			.pName = name.data(),
			.pSpecializationInfo = nullptr,
		};
	}
}