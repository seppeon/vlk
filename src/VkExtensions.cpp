#include "vlk/VkExtensions.hpp"
#include <vulkan/vulkan.h>
#include <stdexcept>
#include <cstdint>
#include <vector>

namespace Vlk
{
    auto GetExtensionCount() -> std::size_t
    {
        uint32_t output = 0;
        if (vkEnumerateInstanceExtensionProperties(nullptr, &output, nullptr) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not determine count of vulkan instance extensions.");
        }
        return static_cast<std::size_t>(output);
    }

    auto GetSupportedExtensions(std::size_t extension_count) -> std::set<std::string>
    {
        std::vector<VkExtensionProperties> typed_output(extension_count);
        auto u32_extension_count = static_cast<std::uint32_t>(extension_count);
        if (vkEnumerateInstanceExtensionProperties(nullptr, &u32_extension_count, typed_output.data()) != VK_SUCCESS)
        {
            throw std::runtime_error("Could not determine vulkan instance extensions.");
        }
        std::set<std::string> output;
        for (auto const & v : typed_output)
        {
            output.insert(v.extensionName);
        }
        return output;
    }

    auto GetDebugUtilsExtension() -> std::string
    {
        return VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
    }
}