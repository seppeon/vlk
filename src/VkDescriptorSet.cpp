#include "vlk/VkDescriptorSet.hpp"
#include <stdexcept>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	auto GetUniformPoolSize(std::size_t descriptor_count) -> VkDescriptorPoolSize
	{
		return VkDescriptorPoolSize{
			.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			.descriptorCount = static_cast<std::uint32_t>(descriptor_count),
		};
	}
	auto CreateDescriptorSetLayout(VkDevice logical_device, VkDescriptorSetLayoutCreateInfo const & create_info, VkAllocationCallbacks const * allocator) -> DescriptorSetLayout
	{
		auto output = DescriptorSetLayout{logical_device, allocator};
		if (vkCreateDescriptorSetLayout(logical_device, &create_info, allocator, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("unable to create the descriptor set layout.");
		}
		return output;
	}
	auto CreateDescriptorSetLayout(VkDevice logical_device, VkDescriptorSetLayoutCreateFlags set_layout_create_flags, Hz::Span<VkDescriptorSetLayoutBinding const> bindings, VkAllocationCallbacks const * allocator) -> DescriptorSetLayout
	{
		auto const set_layout_create_info = VkDescriptorSetLayoutCreateInfo
		{
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
			.pNext = nullptr,
			.flags = set_layout_create_flags,
			.bindingCount = static_cast<std::uint32_t>(bindings.Size()),
			.pBindings = bindings.Data(),
		};
		return CreateDescriptorSetLayout(logical_device, set_layout_create_info, allocator);
	}
	auto CreateDescriptorPool(VkDevice logical_device, VkDescriptorPoolCreateInfo const & create_info, VkAllocationCallbacks const * allocator) -> DescriptorPool
	{
		auto output = DescriptorPool{logical_device, allocator};
		if (vkCreateDescriptorPool(logical_device, &create_info, allocator, &output) != VK_SUCCESS)
		{
			throw std::runtime_error("unable to create the descriptor pool.");
		}
		return output;
	}
	auto CreateDescriptorPool(VkDevice logical_device, VkDescriptorPoolCreateFlags pool_create_flags, std::uint32_t max_sets, Hz::Span<VkDescriptorPoolSize const> pool_sizes, const VkAllocationCallbacks * allocator) -> DescriptorPool
	{
		auto const pool_create_info = VkDescriptorPoolCreateInfo{
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
			.pNext = nullptr,
			.flags = pool_create_flags,
			.maxSets = max_sets,
			.poolSizeCount = static_cast<std::uint32_t>(pool_sizes.Size()),
			.pPoolSizes = pool_sizes.Data(),
		};
		return CreateDescriptorPool(logical_device, pool_create_info, allocator);
	}
	auto AllocateDescriptorSets(VkDevice logical_device, VkDescriptorSetAllocateInfo const & create_info, VkAllocationCallbacks const * allocator) -> DescriptorSets
	{
		auto output = DescriptorSets(logical_device, create_info.descriptorPool, create_info.descriptorSetCount, allocator);
		if (vkAllocateDescriptorSets(logical_device, &create_info, output.data()) != VK_SUCCESS)
		{
			throw std::runtime_error("unable to allocate descriptor sets.");
		}
		return output;
	}
	auto AllocateDescriptorSets(VkDevice logical_device, VkDescriptorPool descriptor_pool, Hz::Span<VkDescriptorSetLayout const> set_layouts, const VkAllocationCallbacks * allocator) -> DescriptorSets
	{
		auto const descriptor_set_allocate_create_info = VkDescriptorSetAllocateInfo{
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
			.pNext = nullptr,
			.descriptorPool = descriptor_pool,
			.descriptorSetCount = static_cast<std::uint32_t>(set_layouts.Size()),
			.pSetLayouts = set_layouts.Data(),
		};
		return AllocateDescriptorSets(logical_device, descriptor_set_allocate_create_info, allocator);
	}

	auto GetWriteUniformDescriptorSet(VkDescriptorSet descriptor_set, std::uint32_t binding, std::uint32_t array_element, Hz::Span<VkDescriptorBufferInfo const> buffer_infos) -> VkWriteDescriptorSet
	{
		return VkWriteDescriptorSet{
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.pNext = nullptr,
			.dstSet = descriptor_set,
			.dstBinding = binding,
			.dstArrayElement = array_element,
			.descriptorCount = static_cast<std::uint32_t>(buffer_infos.Size()),
			.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			.pImageInfo = nullptr,
			.pBufferInfo = buffer_infos.Data(),
			.pTexelBufferView = nullptr,
		};
	}

	void UpdateDescriptorSet(VkDevice logical_device, Hz::Span<VkWriteDescriptorSet const> descriptor_writes, Hz::Span<VkCopyDescriptorSet const> descriptor_copies)
	{
		vkUpdateDescriptorSets(logical_device, uint32_t(descriptor_writes.Size()), descriptor_writes.Data(), uint32_t(descriptor_copies.Size()), descriptor_copies.Data());
	}
	void UpdateDescriptorSet(VkDevice logical_device, Hz::Span<VkCopyDescriptorSet const> descriptor_copies)
	{
		UpdateDescriptorSet(logical_device, {}, descriptor_copies);
	}
	void UpdateDescriptorSet(VkDevice logical_device, Hz::Span<VkWriteDescriptorSet const> descriptor_writes)
	{
		UpdateDescriptorSet(logical_device, descriptor_writes, {});
	}
}