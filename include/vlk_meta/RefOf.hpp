#pragma once
#include <type_traits>
#include <concepts>

namespace Vlk
{

	//
	// | From   | To     | Result |
	// | ------ | ------ | ------ |
	// | const  | mut    | false  |
	// | const  | const  | true   |
	// | mut    | mut    | true   |
	// | mut    | const  | true   |
	template <bool FromConst, bool ToConst>
	inline constexpr bool is_lessar_const_v = not (FromConst and not ToConst);
	template <typename T, bool Const>
	concept Ref = std::is_reference_v<T> and is_lessar_const_v<std::is_const_v<std::remove_reference_t<T>>, Const>;
	static_assert(Ref<char &, true>);
	static_assert(Ref<char const &, true>);
	static_assert(Ref<char &, false>);
	static_assert(not Ref<char const &, false>);
	template <typename T, bool Const>
	concept LRef = std::is_lvalue_reference_v<T> and is_lessar_const_v<std::is_const_v<std::remove_reference_t<T>>, Const>;
	template <typename T, bool Const>
	concept RRef = std::is_rvalue_reference_v<T> and is_lessar_const_v<std::is_const_v<std::remove_reference_t<T>>, Const>;

	template <typename T>
	concept ConstLRef = LRef<T, true>;
	template <typename T>
	concept MutLRef = LRef<T, false>;
	template <typename T>
	concept ConstRRef = RRef<T, true>;
	template <typename T>
	concept MutRRef = RRef<T, false>;

	template <typename T, typename M>
	concept RefOf = Ref<T, std::is_const_v<std::remove_reference_t<M>>> and std::same_as<std::remove_cvref_t<T>, std::remove_cvref_t<M>> and is_lessar_const_v<std::is_const_v<std::remove_reference_t<T>>, std::is_const_v<std::remove_reference_t<M>>>;
	static_assert(RefOf<int const &, int const>);
	static_assert(RefOf<int &, int const>);
	static_assert(not RefOf<int const &, int>);
	static_assert(RefOf<int &, int>);
	static_assert(RefOf<int const &&, int const>);
	static_assert(RefOf<int &&, int const>);
	static_assert(not RefOf<int const &&, int>);
	static_assert(RefOf<int &&, int>);

	template <typename T, typename M>
	concept LRefOf = LRef<T, std::is_const_v<std::remove_reference_t<M>>> and std::same_as<std::remove_cvref_t<T>, std::remove_cvref_t<M>> and is_lessar_const_v<std::is_const_v<std::remove_reference_t<T>>, std::is_const_v<std::remove_reference_t<M>>>;
	static_assert(LRefOf<int const &, int const>);
	static_assert(LRefOf<int &, int const>);
	static_assert(not LRefOf<int const &, int>);
	static_assert(LRefOf<int &, int>);
	static_assert(not LRefOf<int const &&, int const>);
	static_assert(not LRefOf<int &&, int const>);
	static_assert(not LRefOf<int const &&, int>);
	static_assert(not LRefOf<int &&, int>);

	template <typename T, typename M>
	concept RRefOf = RRef<T, std::is_const_v<std::remove_reference_t<M>>> and std::same_as<std::remove_cvref_t<T>, std::remove_cvref_t<M>> and is_lessar_const_v<std::is_const_v<std::remove_reference_t<T>>, std::is_const_v<std::remove_reference_t<M>>>;
	static_assert(not RRefOf<int const &, int const>);
	static_assert(not RRefOf<int &, int const>);
	static_assert(not RRefOf<int const &, int>);
	static_assert(not RRefOf<int &, int>);
	static_assert(RRefOf<int const &&, int const>);
	static_assert(RRefOf<int &&, int const>);
	static_assert(not RRefOf<int const &&, int>);
	static_assert(RRefOf<int &&, int>);
}