#pragma once
#include "vlk_meta/TL.hpp"

namespace Vlk
{
	template <typename T>
	struct FnPointerArgsImpl;
	template <typename R, typename ... Args>
	struct FnPointerArgsImpl<R(Args...)> { using type = TL<Args...>; };
	template <typename R, typename ... Args>
	struct FnPointerArgsImpl<R(Args...) noexcept> { using type = TL<Args...>; };
	template <typename R, typename ... Args>
	struct FnPointerArgsImpl<R(*)(Args...)> { using type = TL<Args...>; };
	template <typename R, typename ... Args>
	struct FnPointerArgsImpl<R(*)(Args...) noexcept> { using type = TL<Args...>; };
	template <typename R, typename ... Args>
	struct FnPointerArgsImpl<R(&)(Args...)> { using type = TL<Args...>; };
	template <typename R, typename ... Args>
	struct FnPointerArgsImpl<R(&)(Args...) noexcept> { using type = TL<Args...>; };
	template <typename T>
	using FnPtrArgs = typename FnPointerArgsImpl<T>::type;
}