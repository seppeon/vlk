#pragma once
#include "vlk_meta/FnPtrTraits.hpp"
#include "vlk_meta/IL.hpp"
#include "vlk_meta/TL.hpp"
#include <algorithm>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	template <typename T, typename Destroy>
	struct HandleTraits
	{
		using args_type = FnPtrArgs<Destroy>;
		static constexpr auto t_index = TLIndexOf<args_type, T>;
		using missing_args_storage = TLRemoveAt<args_type, t_index>;
		using lhs_indexes = std::make_index_sequence<t_index>;
		using rhs_indexes = ILAdd<std::make_index_sequence<missing_args_storage::length - t_index>, t_index>;
	};

	template <typename Traits, typename List = typename Traits::missing_args_storage>
	struct HandleStorage;
	template <typename Traits, typename ... Ts>
	struct HandleStorage<Traits, TL<Ts...>>
	{
		using traits = Traits;

		HandleStorage(Ts... args)
			: m_data(args...)
		{}

		template <std::size_t I>
		constexpr decltype(auto) get_data() const noexcept
		{
			return get<I>(m_data);
		}
	private:
		std::tuple<Ts...> m_data;
	};

	template <typename T, auto destroy> requires (std::is_pointer_v<T>)
	class CreatedHandle : HandleStorage<HandleTraits<T, decltype(destroy)>>
	{
		using base = HandleStorage<HandleTraits<T, decltype(destroy)>>;
		using base::get_data;
		using typename base::traits;

		using lhs_indexes = typename traits::lhs_indexes;
		using rhs_indexes = typename traits::rhs_indexes;
		static constexpr auto t_index = traits::t_index;
		T m_value = nullptr;
	public:
		using base::base;
        CreatedHandle& operator=(std::nullptr_t)
		{
        	m_value = nullptr;
			return *this;
		}
        operator bool() const noexcept
		{
			return m_value != nullptr;
		}
		constexpr T* operator&() noexcept
		{
			return &m_value;
		}
		constexpr T const * operator&() const noexcept
		{
			return &m_value;
		}
		constexpr operator T&() noexcept
		{
			return m_value;
		}
		constexpr operator T const &() const noexcept
		{
			return m_value;
		}
		constexpr T const & Get() const noexcept
		{
			return m_value;
		}

		constexpr CreatedHandle(CreatedHandle const & ipp) = delete;
		constexpr CreatedHandle& operator=(CreatedHandle const & ipp) = delete;
		constexpr CreatedHandle(CreatedHandle && ipp)
			: base(std::move(ipp))
			, m_value(std::exchange(ipp.m_value, nullptr))
		{
		}
		constexpr CreatedHandle& operator=(CreatedHandle && ipp)
		{
			if (&ipp == this) return *this;
			std::destroy_at(this);
			std::construct_at(this, std::move(ipp));
			return *this;
		}
		~CreatedHandle()
		{
			if (m_value != nullptr)
			{
				[&, this]<std::size_t ... Ls, std::size_t ... Rs>(std::index_sequence<Ls...>, std::index_sequence<Rs...>)
				{
					destroy(
						base::template get_data<Ls>() ...,
						m_value,
						base::template get_data<Rs>() ...
					);
				}(lhs_indexes{}, rhs_indexes{});
			}
		}
	};
}