#pragma once
#include "vlk_meta/AlwaysInline.hpp"
#include <type_traits>

namespace Vlk
{
    template <typename T> requires (std::is_enum_v<T>)
    [[nodiscard]] ALWAYS_INLINE constexpr auto UnderlyingValue(T value) -> std::underlying_type_t<T>
    {
        return static_cast<std::underlying_type_t<T>>(value);
    }
}