#pragma once
#include <tuple>
#include <cstddef>
#include <type_traits>

namespace Vlk
{
	template <typename...Ts>
	struct TL
	{
		static constexpr auto length = sizeof...(Ts);
	};
	template <typename List, size_t Index>
	struct AtIndexImpl
	{
		using type = void;
	};
	template <template<typename...> class As, typename head, typename ... args>
	struct AtIndexImpl<As<head, args...>, 0>
	{
		using type = head;
	};
	template <template<typename...> class As, size_t i, typename head, typename ... tail>
		requires (i > 0)
	struct AtIndexImpl<As<head, tail ... >, i>
	{
		using tail_list = As<tail ... >;
		using type = typename AtIndexImpl<tail_list, i - 1>::type;
	};
	template <typename List, size_t I>
	using TLAtIndex = typename AtIndexImpl<List, I>::type;

	template <typename List, typename Match, size_t Index = 0>
	struct IndexOfImpl;
	template <template<typename...> class As, typename head, typename ... args, typename Match, size_t Index>
		requires (std::is_same_v<head, Match>)
	struct IndexOfImpl<As<head, args...>, Match, Index>
	{
		static constexpr auto value = Index;
	};
	template <template<typename...> class As, typename head, typename ... tail, typename Match, size_t Index>
	struct IndexOfImpl<As<head, tail ... >, Match, Index>
	{
		static constexpr auto value = IndexOfImpl<As<tail ... >, Match, Index + 1>::value;
	};
	template <typename List, typename Match>
	inline constexpr size_t TLIndexOf = IndexOfImpl<List, Match>::value;

	template <typename Lst, size_t I, typename Buf = TL<>>
	struct RemoveAtImpl;
	template <template<typename...> class As, size_t I, typename ... Tail>
	struct RemoveAtImpl<As<>, I, As<Tail...>>
	{
		using type = As<Tail...>;
	};
	template <template<typename...> class As, typename First, typename ... Args, typename ... Tail>
	struct RemoveAtImpl<As<First, Args...>, 0, As<Tail...>>
	{
		using type = As<Tail..., Args...>;
	};
	template <template<typename...> class As, typename First, typename ... Args, size_t I, typename ... Tail>
		requires (I != 0)
	struct RemoveAtImpl<As<First, Args...>, I, As<Tail...>>
	{
		using type = typename RemoveAtImpl<As<Args...>, (I - 1), As<Tail..., First>>::type;
	};
	template <typename List, size_t I>
	using TLRemoveAt = typename RemoveAtImpl<List, I>::type;

	template <typename Lst>
	struct RemoveFrontImpl
	{
		using type = Lst;
	};
	template <template<typename...> class As, typename Ff, typename ... Ts>
	struct RemoveFrontImpl<As<Ff, Ts...>>
	{
		using type = As<Ts...>;
	};
	template <typename List>
	using TLRemoveFront = typename RemoveFrontImpl<List>::type;

	template <typename Lst, typename M>
	struct TLContainsImpl;
	template <typename ... Args, typename M>
	struct TLContainsImpl<TL<Args...>, M>
	{
		static constexpr bool value = ( std::is_same_v<Args, M> or ... );
	};
	template <typename Lst, typename M>
	concept TLContains = TLContainsImpl<Lst, M>::value;

	template <typename Lhs, typename Rhs, typename Out = TL<>>
	struct TLCommonImpl
	{
		using type = Out;
	};
	template <typename Lhs, template<typename...> class R, typename Rf, typename ... Rs, template<typename...> class O, typename ... Os>
		requires (not TLContains<Lhs, Rf>)
	struct TLCommonImpl<Lhs, R<Rf, Rs...>, O<Os...>>
	{
		using type = typename TLCommonImpl<Lhs, R<Rs...>, O<Os...>>::type;
	};
	template <typename Lhs, template<typename...> class R, typename Rf, typename ... Rs, template<typename...> class O, typename ... Os>
		requires (TLContains<Lhs, Rf>)
	struct TLCommonImpl<Lhs, R<Rf, Rs...>, O<Os...>>
	{
		using type = typename TLCommonImpl<Lhs, R<Rs...>, O<Os..., Rf>>::type;
	};
	template <typename Lhs, typename Rhs>
	using TLCommon = typename TLCommonImpl<Lhs, Rhs>::type;

	template <std::size_t I, typename Lhs, typename Rhs, typename Out = std::index_sequence<>>
	struct TLCommonIsImpl
	{
		using type = Out;
	};
	template <
			std::size_t I,
			typename T,
			template<typename...> class L, typename ... Ls,
			template<typename...> class R, typename ... Rs,
			std::size_t ... Os
		>
	struct TLCommonIsImpl<I, L<T, Ls...>, R<T, Rs...>, std::index_sequence<Os...>>
	{
		using type = typename TLCommonIsImpl<
			I + 1,
			L<Ls...>,
			R<Rs...>,
			std::index_sequence<Os..., I>
		>::type;
	};
	template <
			std::size_t I,
			template<typename...> class L, typename Lf, typename ... Ls,
			template<typename...> class R, typename Rf, typename ... Rs,
			std::size_t ... Os
		>
	struct TLCommonIsImpl<I, L<Lf, Ls...>, R<Rf, Rs...>, std::index_sequence<Os...>>
	{
		using type = typename TLCommonIsImpl<
			I + 1,
			L<Ls...>,
			R<Rf, Rs...>,
			std::index_sequence<Os...>
		>::type;
	};
	template <std::size_t I, typename Lhs, typename Rhs>
	using TLCommonIs = typename TLCommonIsImpl<I, Lhs, Rhs>::type;

	template <typename Lst, typename M, typename O = TL<>>
	struct TLRemoveMImpl { using type = O; };
	template <typename ... Ts, typename M, template<typename...> class O, typename ... Os>
	struct TLRemoveMImpl<TL<M, Ts...>, M, O<Os...>> { using type = TL<Os..., Ts...>; };
	template <typename Tf, typename ... Ts, typename M, template<typename...> class O, typename ... Os>
	struct TLRemoveMImpl<TL<Tf, Ts...>, M, O<Os...>> { using type = typename TLRemoveMImpl<TL<Ts...>, M, O<Os..., Tf>>::type; };
	template <typename Haystack, typename Needle>
	using TLRemoveM = typename TLRemoveMImpl<Haystack, Needle>::type;

	template <typename Haystack, typename Needle>
	struct TLRemoveImpl;
	template <typename Haystack>
	struct TLRemoveImpl<Haystack, TL<>> { using type = Haystack; };
	template <typename Haystack, template <typename ...> class R, typename Rf, typename ... Rs>
	struct TLRemoveImpl<Haystack, R<Rf, Rs...>> { using type = typename TLRemoveImpl<TLRemoveM<Haystack, Rf>, R<Rs...>>::type; };
	template <typename Haystack, typename Needle>
	using TLRemove = typename TLRemoveImpl<Haystack, Needle>::type;

	template <typename Lst>
	struct TLToTupleImpl;
	template <typename ... Ts>
	struct TLToTupleImpl<TL<Ts...>> { using type = std::tuple<Ts...>; };
	template <typename Lst>
	using TLToTuple = typename TLToTupleImpl<Lst>::type;

	template <typename Lhs, std::size_t I, typename Rhs = TL<>>
	struct TLHalvesImpl;
	template <typename...Ls, typename...Rs>
	struct TLHalvesImpl<TL<Ls...>, 0, TL<Rs...>>
	{
		using lhs = TL<Rs...>;
		using rhs = TL<Ls...>;
	};
	template <typename Lf, typename...Ls, std::size_t I, typename...Rs>
		requires (I != 0)
	struct TLHalvesImpl<TL<Lf, Ls...>, I, TL<Rs...>>
		: TLHalvesImpl<TL<Ls...>, I - 1, TL<Rs..., Lf>>
	{};
	template <typename List, std::size_t I>
	using TLLeft = typename TLHalvesImpl<List, I>::lhs;
	template <typename List, std::size_t I>
	using TLRight = typename TLHalvesImpl<List, I>::rhs;
}