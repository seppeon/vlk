#pragma once

#include <utility>
namespace Vlk
{
	template <typename List, std::size_t D>
	struct ILAddImpl;
	template <std::size_t ... Is, std::size_t D>
	struct ILAddImpl<std::index_sequence<Is...>, D>
	{
		using type = std::index_sequence<(Is + D)...>;
	};
	template <typename List, std::size_t D>
	using ILAdd = typename ILAddImpl<List, D>::type;
}