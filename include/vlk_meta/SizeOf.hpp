#pragma once
#include <array>
#include <type_traits>

namespace Vlk
{
	template <typename T>
	concept TriviallyCopyConstructible = std::is_trivially_copy_constructible_v<std::remove_cvref_t<T>>;

	template <typename T>
	concept Pointer = std::is_pointer_v<std::remove_cvref_t<T>>;

	template <typename T>
	concept PointerToTriviallyCopyConstructible = Pointer<T> and requires(T obj)
	{
		{ *obj } -> TriviallyCopyConstructible;
	};

	template <typename T>
	concept ContigiousContainer = requires(T obj)
	{
		{ std::data(obj) } -> PointerToTriviallyCopyConstructible;
		{ std::size(obj) } -> std::same_as<std::size_t>;
	};

	template <typename T>
	[[nodiscard]] constexpr auto SizeOf(T const & container) -> std::size_t
	{
		return sizeof(container);
	}

	template <ContigiousContainer T>
	[[nodiscard]] constexpr auto SizeOf(T const & container) -> std::size_t
	{
		return std::size(container) * sizeof(*std::data(container));
	}
}