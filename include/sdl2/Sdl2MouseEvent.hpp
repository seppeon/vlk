#pragma once
#include "Sdl2EventCommon.hpp"
#include "Sdl2Button.hpp"
#include <variant>
#include <cstdint>

namespace Sdl2
{
	enum struct MouseButton : std::uint32_t
	{
		Left,
		Middle,
		Right,
		X1,
		X2,
	};

	struct MouseButtonEvent
	{
		MouseButton button;
		Button event;
	};

	struct MouseEvent
	{
		struct Motion : EventVec2df {};
		struct Wheel : EventVec2df {};
		std::uint32_t mouse_instance_id = 0;
		EventVec2d coordinate;
		std::variant
		<
			Motion,
			Wheel,
			MouseButtonEvent
		> event_data;
	};
}