#pragma once
#include <filesystem>
#include <string>
#include <variant>

namespace Sdl2
{
	// Window -> Drop
	struct DropEvent
	{
		struct File{ std::filesystem::path path; };
		struct Text{ std::u8string text; };
		struct Begin{};
		struct Complete{};
		std::variant<File, Text, Begin, Complete> event_data;
	};
}