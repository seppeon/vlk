#pragma once
#include <cstdint>

extern "C"
{
	struct SDL_image;
	struct SDL_vulkan;
	struct SDL_Window;
	struct SDL_Surface;
};

namespace Sdl2
{
	struct WindowFocus
	{
		bool input_focus;
		bool mouse_focus;
	};

	enum class WindowType : std::uint32_t
	{
		Normal,
		Tooltip,
		Utility,
		PopupMenu,
	};

	struct WindowFlags
	{
		bool hidden = false;
		bool fullscreen = false;
		bool always_on_top = false;
		bool borderless = false;
		bool resizable = true;
		bool mouse_capture = false;
		bool skip_taskbar = false;
		bool mouse_grabbed = false;
		bool keyboard_grabbed = false;
	};

	enum class WindowDisplayState : std::uint32_t
	{
		Restored,
		Minimised,
		Maximised,
	};

	struct WindowGraphics
	{
		bool opengl = false;
		bool vulkan = false;
	};

	struct WindowSettings
	{
		WindowType type = WindowType::Normal;
		WindowFlags flags = {};
		WindowDisplayState display_state = WindowDisplayState::Restored;
		WindowGraphics graphics = {};
	};

	struct Centered{};

	struct WindowPosition
	{
		bool centered = true;
		std::uint32_t value = 0;

		constexpr WindowPosition(auto v)
			: centered(IsCentered(v))
			, value(Value(v))
		{}
	private:
		static constexpr std::uint32_t Value(std::uint32_t const & v) noexcept { return v; }
		static constexpr std::uint32_t Value(Centered const & v) noexcept { return 0; }
		static constexpr auto IsCentered(std::uint32_t const & v) noexcept { return false; }
		static constexpr auto IsCentered(Centered const & v) noexcept { return true; }
	};

	struct WindowDrawableSize
	{
		std::uint32_t width;
		std::uint32_t height;
	};

	struct WindowExtents
	{
		WindowPosition x = { Centered{} };
		WindowPosition y = { Centered{} };
		std::uint32_t width = 360;
		std::uint32_t height = 240;
	};

	enum class WindowFlashType : std::uint32_t
	{
		Cancel,
		Briefly,
		UntilFocused,
	};
}