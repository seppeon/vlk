#pragma once
#include "Sdl2/Sdl2Event.hpp"
#include <Hz/Span.hpp>

namespace Sdl2
{
    bool DefaultEventHandler(Hz::Span<Sdl2::Event const> events);

    std::string DescribeEvent(Sdl2::Event const & event);
}