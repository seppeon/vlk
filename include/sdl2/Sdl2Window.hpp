#pragma once
#include "vlk_meta/Handle.hpp"
#include "Sdl2/Types.hpp"
#include "Sdl2/Sdl2Init.hpp"
#include <string>
#include <memory>
#include <vector>
#include <set>
#include <filesystem>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Sdl2
{
	using SurfaceKHR = Vlk::CreatedHandle<VkSurfaceKHR, vkDestroySurfaceKHR>;
	struct Sdl2Window
	{
		Sdl2Window(std::string title, WindowExtents extents, WindowSettings const & settings = {}) noexcept;

		Sdl2Window& operator=(Sdl2Window &&);
		Sdl2Window& operator=(Sdl2Window const &) = delete;
		Sdl2Window(Sdl2Window &&);
		Sdl2Window(Sdl2Window const &) = delete;

		~Sdl2Window();

		[[nodiscard]] WindowType GetWindowType() const;
		[[nodiscard]] WindowFocus GetWindowFocus() const;

		[[nodiscard]] std::string GetTitle() const;
		bool SetTitle(std::string title);

		[[nodiscard]] WindowExtents GetExtents() const;
		bool SetExtents(WindowExtents extents);

		[[nodiscard]] WindowDrawableSize GetDrawableSize() const;

		[[nodiscard]] WindowDisplayState GetWindowDisplayState() const;
		bool SetWindowDisplayState(WindowDisplayState state);

		[[nodiscard]] WindowFlags GetWindowFlags() const;
		bool SetWindowFlags(WindowFlags flags);

		[[nodiscard]] WindowGraphics GetWindowGraphics() const;
		bool SetWindowGraphics(WindowGraphics graphics);

		bool FlashWindow(WindowFlashType type);

		bool RaiseWindow();

		bool SetWindowIcon(std::filesystem::path path);

		[[nodiscard]] auto GetVulkanSurface(VkInstance instance) const -> SurfaceKHR;
		[[nodiscard]] auto GetVulkanExtensions() const -> std::set<std::string>;
	private:
		std::unique_ptr<SDL_Window, void(*)(SDL_Window*)> m_window_ptr;
		std::unique_ptr<SDL_Surface, void(*)(SDL_Surface*)> m_icon_ptr;

		Sdl2Init m_sdl2_init{ Sdl2Subsystem::Video };
	};
}