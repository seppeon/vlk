#pragma once
#include <vulkan/vulkan.h>
#include <string>

namespace Vlk
{
	[[nodiscard]] auto ToString(VkMemoryType const & memory_type) -> std::string;
}