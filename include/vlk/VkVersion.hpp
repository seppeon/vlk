#pragma once
#include <FL/Semver.hpp>

namespace Vlk
{
    [[nodiscard]] auto ApiVersion() -> FL::Semver;

    [[nodiscard]] auto ToVkVersion(FL::Semver const & semver) -> std::uint32_t;
}