#pragma once
#include "vlk/VkUtil.hpp"
#include <climits>
#include <concepts>
#include <cstddef>
#include <glm/glm.hpp>
#include <type_traits>
#include <cstdint>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	template <typename T>
	concept DataMember = std::is_member_object_pointer_v<T>;

	enum class TypeCategory
	{
		Uint,
		Int,
		Float,
		Object,
	};

	template <typename T>
	[[nodiscard]] consteval auto GetTypeCategory() noexcept
	{
		if constexpr (std::is_unsigned_v<T>)
		{
			return TypeCategory::Uint;
		}
		if constexpr (std::is_signed_v<T>)
		{
			return TypeCategory::Int;
		}
		if constexpr (std::is_floating_point_v<T>)
		{
			return TypeCategory::Float;
		}
		if constexpr (std::is_object_v<T>)
		{
			return TypeCategory::Object;
		}
	}

	template <typename T>
	struct DataMemberTraitsImpl;
	template <typename Obj, typename T>
	struct DataMemberTraitsImpl<T Obj::*>
	{
		using member_type = T;
		using object_type = Obj;
	};

	template <typename T>
	using get_data_member_type = typename DataMemberTraitsImpl<T>::member_type;

	template <typename T>
	using get_object_type = typename DataMemberTraitsImpl<T>::object_type;

	template <typename ... Ts>
	struct GetFirstTypeImpl;
	template <typename F, typename ... Ts>
	struct GetFirstTypeImpl<F, Ts...>
	{
		using type = F;
	};

	template <typename ... Ts>
	using get_first_type = typename GetFirstTypeImpl<Ts...>::type;

	template <typename T>
	struct VkUserDefinedFormat;

	template <std::size_t N>
	struct VkDataMemberDetails
	{
		std::array<VkFormat, N> formats;
		std::array<std::size_t, N> offsets;

		[[nodiscard]] constexpr auto size() const noexcept -> std::size_t
		{
			return N;
		}
	};

	template <typename T>
	inline constexpr bool is_vk_data_member_details_v = false;
	template <std::size_t N>
	inline constexpr bool is_vk_data_member_details_v<VkDataMemberDetails<N>> = true;
	template <typename T>
	concept IsVkDataMemberDetails = is_vk_data_member_details_v<std::remove_cvref_t<T>>;

	template <typename T>
	concept VkHasUserDefinedFormatDescription = requires
	{
		{ VkUserDefinedFormat<std::remove_cvref_t<T>>::format } -> IsVkDataMemberDetails;
	};
	/**
	 * @brief Provides the VkFormat to use for any given data member type.
	 *
	 * @note If there is not pre-programmed data type, you can provide one with a specialization of `VkUserDefinedFormat`.
	 *
	 * This has built-in support for glm's typedef's and the appropreate C++ builtin types.
	 *
	 * @tparam T The type of the object.
	 * @tparam Member The type of the data member.
	 * @return The format.
	 */
    template <class T, class Member>
    [[nodiscard]] consteval auto VkGetDataMemberType(Member T::* data_member)
	{
		auto const base_offset = OffsetOfDataMember(data_member) * 8;
		auto apply_offset = [=](auto & offsets)
		{
			for (auto & offset : offsets)
			{
				offset += base_offset;
				offset /= 8;
			}
		};
		if constexpr (VkHasUserDefinedFormatDescription<T>)
		{
			auto output = VkUserDefinedFormat<std::remove_cvref_t<T>>::format;
			apply_offset(output.offsets);
			return output;
		}
		else
		{
			auto output = []
			{
				if constexpr (std::is_same_v<Member, double>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R64_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, float>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32_SFLOAT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_unsigned_v<Member> and ((sizeof(Member) * CHAR_BIT) == 8))
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R8_UINT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_same_v<Member, std::uint16_t>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R16_UINT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_same_v<Member, std::uint32_t>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32_UINT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_same_v<Member, std::uint64_t>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R64_UINT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_signed_v<Member> and ((sizeof(Member) * CHAR_BIT) == 8))
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R8_SINT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_same_v<Member, std::int16_t>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R16_SINT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_same_v<Member, std::int32_t>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32_SINT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_same_v<Member, std::int64_t>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R64_SINT },
						.offsets = { 0 }
					};
				}
				if constexpr (std::is_same_v<Member, glm::ivec2>)
				{
					return VK_FORMAT_R32G32_SINT;
				}
				if constexpr (std::is_same_v<Member, glm::ivec3>)
				{
					return VK_FORMAT_R32G32B32_SINT;
				}
				if constexpr (std::is_same_v<Member, glm::ivec4>)
				{
					return VK_FORMAT_R32G32B32A32_SINT;
				}
				if constexpr (std::is_same_v<Member, glm::uvec2>)
				{
					return VK_FORMAT_R32G32_UINT;
				}
				if constexpr (std::is_same_v<Member, glm::uvec3>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32G32B32_UINT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::uvec4>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32G32B32A32_UINT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::vec2>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32G32_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::vec3>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32G32B32_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::vec4>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32G32B32A32_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dvec2>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R64G64_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dvec3>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R64G64B64_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dvec4>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R64G64B64A64_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat2>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R64G64B64A64_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat2x2>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R64G64B64A64_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat2>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32G32B32A32_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat2x2>)
				{
					return VkDataMemberDetails<1>{
						.formats = { VK_FORMAT_R32G32B32A32_SFLOAT },
						.offsets = { 0 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat2x3>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64_SFLOAT,
							VK_FORMAT_R64G64B64_SFLOAT,
						},
						.offsets = { 0, 64*3 }
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat2x4>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
						},
						.offsets = { 0, 64*4 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat3>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64_SFLOAT,
							VK_FORMAT_R64G64B64_SFLOAT,
							VK_FORMAT_R64G64B64_SFLOAT,
						},
						.offsets = { 0, 64*4 * 1, 64*4*2 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat3x2>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64_SFLOAT,
							VK_FORMAT_R64G64B64_SFLOAT,
						},
						.offsets = { 0, 64*4 * 1 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat3x3>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64_SFLOAT,
							VK_FORMAT_R64G64B64_SFLOAT,
							VK_FORMAT_R64G64B64_SFLOAT,
						},
						.offsets = { 0, 64*3*1, 64*3*2 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat3x4>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
						},
						.offsets = { 0, 64*4*1, 64*4*2 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat4>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
						},
						.offsets = { 0, 64*4*1, 64*4*2, 64*4*3 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat4x2>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
						},
						.offsets = { 0, 64*4*1 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat4x3>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
						},
						.offsets = { 0, 64*4*1, 64*4*2 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::dmat4x4>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
							VK_FORMAT_R64G64B64A64_SFLOAT,
						},
						.offsets = { 0, 64*4*1, 64*4*2, 64*4*3 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat2x3>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32_SFLOAT,
							VK_FORMAT_R32G32B32_SFLOAT,
						}
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat2x4>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
						},
						.offsets = { 0, 32*4*1 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat3>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32_SFLOAT,
							VK_FORMAT_R32G32B32_SFLOAT,
							VK_FORMAT_R32G32B32_SFLOAT,
						},
						.offsets = { 0, 32*3*1, 32*3*2 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat3x2>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32_SFLOAT,
							VK_FORMAT_R32G32B32_SFLOAT,
						},
						.offsets = { 0, 32*3*1 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat3x3>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32_SFLOAT,
							VK_FORMAT_R32G32B32_SFLOAT,
							VK_FORMAT_R32G32B32_SFLOAT,
						},
						.offsets = { 0, 32*3*1, 32*3*2 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat3x4>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
						},
						.offsets = { 0, 32*4*1, 32*4*2 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat4>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
						},
						.offsets = { 0, 32*4*1, 32*4*2, 32*4*3 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat4x2>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
						},
						.offsets = { 0, 32*4*1 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat4x3>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
						},
						.offsets = { 0, 32*4*1, 32*4*2 },
					};
				}
				if constexpr (std::is_same_v<Member, glm::mat4x4>)
				{
					return VkDataMemberDetails
					{
						.formats = std::array{
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
							VK_FORMAT_R32G32B32A32_SFLOAT,
						},
						.offsets = { 0, 32*4*1, 32*4*2, 32*4*3 },
					};
				}
			}();
			apply_offset(output.offsets);
			return output;
		}
	}
	
	template <std::size_t ... Ns>
	[[nodiscard]] consteval auto VkConcatFormats(VkDataMemberDetails<Ns> const & ... details) -> VkDataMemberDetails<(Ns + ...)>
	{
		return VkDataMemberDetails<(Ns + ...)>{
			.formats = ConcatArrays(details.formats ...),
			.offsets = ConcatArrays(details.offsets ...),
		};
	}

	template <std::uint32_t binding, VkVertexInputRate input_rate, DataMember auto ... Members>
	class VkVertexDescription
	{
		using first_member_type = get_first_type<decltype(Members)...>;
		using object_type = get_object_type<first_member_type>;
		static constexpr auto vk_format = VkConcatFormats( VkGetDataMemberType(Members) ... );
		using attribute_description_type = std::array<VkVertexInputAttributeDescription, vk_format.size()>;
	public:
		static constexpr auto binding_description = VkVertexInputBindingDescription{
			.binding = binding,
			.stride = sizeof(object_type),
			.inputRate = input_rate,
		};
		static constexpr auto attribute_descriptions = []() -> attribute_description_type
		{
			attribute_description_type output{};
			for (std::uint32_t i = 0; i < vk_format.size(); ++i)
			{
				auto & desc = output[i];
				desc.location = i;
				desc.binding = binding;
				desc.format = vk_format.formats[i];
				desc.offset = vk_format.offsets[i];
			}
			return output;
		}();
	};

	struct TestFoo { int a; glm::mat4 b; float c; };
	inline constexpr auto test_foo_vertex_info = VkVertexDescription<
		0,
		VK_VERTEX_INPUT_RATE_VERTEX,
		&TestFoo::a, &TestFoo::b, &TestFoo::c
	>{};
	static_assert(test_foo_vertex_info.attribute_descriptions.size() == 6);
	static_assert(test_foo_vertex_info.attribute_descriptions[0].offset == offsetof(TestFoo, a));
	static_assert(test_foo_vertex_info.attribute_descriptions[1].offset == offsetof(TestFoo, b));
	static_assert(test_foo_vertex_info.attribute_descriptions[2].offset == offsetof(TestFoo, b) + sizeof(float)*4);
	static_assert(test_foo_vertex_info.attribute_descriptions[5].offset == offsetof(TestFoo, c));
}