#pragma once
#include "vlk/VkHandles.hpp"
#include <vulkan/vulkan.h>
#include <Hz/Span.hpp>
#include <string>
#include <set>

namespace Vlk
{
	[[nodiscard]] auto CreateLogicalDevice(VkPhysicalDevice const & physical_device, std::set<std::string> const & extensions, Hz::Span<VkDeviceQueueCreateInfo const> queue_families) -> Device;

	[[nodiscard]] auto CreateLogicalDevice(VkPhysicalDevice const & physical_device, std::set<std::string> const & extensions, Hz::Span<std::uint32_t const> device_queues, float const & priority) -> Device;
}