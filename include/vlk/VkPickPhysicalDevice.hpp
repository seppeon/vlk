#pragma once
#include "Vlk/VkPhysicalDevice.hpp"
#include <vulkan/vulkan.h>
#include <cstdint>
#include <Hz/Span.hpp>

namespace Vlk
{
    [[nodiscard]] auto PickPhysicalDevice(Hz::Span<VkPhysicalDevice const> devices) -> VkPhysicalDevice const *;

    [[nodiscard]] auto PickPresentMode(SwapChainSupportDetails const & support_details) -> VkPresentModeKHR;

    [[nodiscard]] auto PickSurfaceFormat(SwapChainSupportDetails const & support_details) -> VkSurfaceFormatKHR;

    [[nodiscard]] auto PickSwapExtents(SwapChainSupportDetails const & support_details, std::uint32_t x_pixels, std::uint32_t y_pixels) -> VkExtent2D;

    [[nodiscard]] auto PickImageCount(SwapChainSupportDetails const & support_details, std::uint32_t desired_extra_images_than_minimum = 1) -> std::uint32_t;
}