#pragma once
#include <cstddef>
#include <vector>
#include <string>
#include <vulkan/vulkan.h>

namespace Vlk
{
    [[nodiscard]] auto GetInstanceLayersCount() -> std::size_t;
    [[nodiscard]] auto GetInstanceLayers(std::size_t layer_count = GetInstanceLayersCount()) -> std::vector<VkLayerProperties>;
    [[nodiscard]] auto HasInstanceLayer(std::string name, std::vector<VkLayerProperties> const & layers) -> bool;
}