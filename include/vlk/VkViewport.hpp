#pragma once
#include <vulkan/vulkan.hpp>
#include <cstdint>
#include <Hz/Span.hpp>

namespace Vlk
{
	[[nodiscard]] auto GetViewport(float x, float y, float width, float height, float min_depth = 0, float max_depth = 1) -> VkViewport;

	[[nodiscard]] auto GetScissor(std::int32_t x, std::int32_t y, std::uint32_t width, std::uint32_t height) -> VkRect2D;

	[[nodiscard]] auto GetViewportCreateInfo(Hz::Span<VkViewport const> viewports, Hz::Span<VkRect2D const> scissors) -> VkPipelineViewportStateCreateInfo;
}