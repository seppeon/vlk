#pragma once
#include "vlk_meta/Handle.hpp"
#include <vector>
#include <cstdint>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	using CommandPool = CreatedHandle<VkCommandPool, vkDestroyCommandPool>;
	using Instance = CreatedHandle<VkInstance, vkDestroyInstance>;
	using FrameBuffer = CreatedHandle<VkFramebuffer, vkDestroyFramebuffer>;
	using Device = CreatedHandle<VkDevice, vkDestroyDevice>;
	using PipelineLayout = CreatedHandle<VkPipelineLayout, vkDestroyPipelineLayout>;
	using RenderPass = CreatedHandle<VkRenderPass, vkDestroyRenderPass>;
	using Pipeline = CreatedHandle<VkPipeline, vkDestroyPipeline>;
	using ShaderModule = CreatedHandle<VkShaderModule, vkDestroyShaderModule>;
	using SwapChain = CreatedHandle<VkSwapchainKHR, vkDestroySwapchainKHR>;
	using ImageView = CreatedHandle<VkImageView, vkDestroyImageView>;
	using DebugUtilsMessengerEXT = CreatedHandle<VkDebugUtilsMessengerEXT, vkDestroyDebugUtilsMessengerEXT>;
	using Semaphore = CreatedHandle<VkSemaphore, vkDestroySemaphore>;
	using Fence = CreatedHandle<VkFence, vkDestroyFence>;
	using Buffer = CreatedHandle<VkBuffer, vkDestroyBuffer>;
	using DescriptorPool = CreatedHandle<VkDescriptorPool, vkDestroyDescriptorPool>;
	using DescriptorSetLayout = CreatedHandle<VkDescriptorSetLayout, vkDestroyDescriptorSetLayout>;

	struct CommandBuffers
	{
		CommandBuffers() = delete;
		CommandBuffers(CommandBuffers const &) = delete;
		CommandBuffers& operator=(CommandBuffers const &) = delete;
		CommandBuffers(VkDevice device, VkCommandPool command_pool, std::size_t buffer_count);
		CommandBuffers(CommandBuffers && command_buffers);
		CommandBuffers& operator=(CommandBuffers && command_buffers);
		~CommandBuffers();

		[[nodiscard]] auto data() noexcept -> VkCommandBuffer *;
		[[nodiscard]] auto data() const noexcept -> VkCommandBuffer const *;
		[[nodiscard]] auto size() const noexcept -> std::uint32_t;
		[[nodiscard]] auto front() const noexcept -> VkCommandBuffer;
		[[nodiscard]] auto back() const noexcept -> VkCommandBuffer;
	private:
		VkDevice m_device = nullptr;
		VkCommandPool m_command_pool = nullptr;
		std::vector<VkCommandBuffer> m_buffers;
	};

	struct DescriptorSets
	{
		DescriptorSets() = delete;
		DescriptorSets(DescriptorSets const &) = delete;
		DescriptorSets& operator=(DescriptorSets const &) = delete;
		DescriptorSets(VkDevice logical_device, VkDescriptorPool descriptor_pool, std::size_t descriptor_set_count, VkAllocationCallbacks const * allocator = nullptr);
		DescriptorSets(DescriptorSets && command_buffers);
		DescriptorSets& operator=(DescriptorSets && command_buffers);
		~DescriptorSets();

		[[nodiscard]] auto data() noexcept -> VkDescriptorSet *;
		[[nodiscard]] auto data() const noexcept -> VkDescriptorSet const *;
		[[nodiscard]] auto size() const noexcept -> std::uint32_t;
		[[nodiscard]] auto front() const noexcept -> VkDescriptorSet;
		[[nodiscard]] auto back() const noexcept -> VkDescriptorSet;

		[[nodiscard]] auto begin() const noexcept -> VkDescriptorSet const *;
		[[nodiscard]] auto end() const noexcept -> VkDescriptorSet const *;
		[[nodiscard]] auto operator[](std::size_t i) const noexcept -> VkDescriptorSet;
	private:
		VkDevice m_device = nullptr;
		VkDescriptorPool m_descriptor_pool = nullptr;
		VkAllocationCallbacks const * m_allocator = nullptr;
		std::vector<VkDescriptorSet> m_descriptor_sets;
	};
}