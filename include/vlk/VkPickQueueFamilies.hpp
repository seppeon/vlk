#pragma once
#include <cstdint>
#include <Hz/Span.hpp>
#include <vector>
#include <optional>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
    struct QueueFamilies
    {
        std::optional<std::uint32_t> graphics_family;
        std::optional<std::uint32_t> present_family;
        std::optional<std::uint32_t> compute_family;
        std::optional<std::uint32_t> transfer_family;
    };

    [[nodiscard]] auto PickQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface, Hz::Span<VkQueueFamilyProperties const> queue_family_properties) -> QueueFamilies;

    struct DeviceQueue
    {
        std::size_t index;
        VkQueue queue;
    };

    [[nodiscard]] bool operator<(DeviceQueue const & lhs, DeviceQueue const & rhs);

    [[nodiscard]] auto GetUniqueQueues(Hz::Span<std::uint32_t const> device_queues) -> std::vector<std::uint32_t>;

    [[nodiscard]] auto GetQueueCreateInfo(Hz::Span<std::uint32_t const> device_queues, float const & priority) -> std::vector<VkDeviceQueueCreateInfo>;

    [[nodiscard]] auto GetDeviceGraphicsQueue(VkDevice device, QueueFamilies const & queue_families) -> DeviceQueue;

    [[nodiscard]] auto GetDevicePresentQueue(VkDevice device, QueueFamilies const & queue_families) -> DeviceQueue;

    [[nodiscard]] auto GetDeviceComputeQueue(VkDevice device, QueueFamilies const & queue_families) -> DeviceQueue;

    [[nodiscard]] auto GetDeviceTransferQueue(VkDevice device, QueueFamilies const & queue_families) -> DeviceQueue;
}