#pragma once
#include <string>
#include <FL/Semver.hpp>

namespace Vlk
{
    struct Versioned
    {
        std::string name;
        FL::Semver version;
    };
}