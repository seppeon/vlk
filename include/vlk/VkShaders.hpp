#pragma once
#include "vlk/VkHandles.hpp"
#include <vulkan/vulkan.h>
#include <filesystem>
#include <Hz/Span.hpp>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	[[nodiscard]] auto CreateShaderFromCode(VkDevice device, Hz::Span<char const> code) -> ShaderModule;

	[[nodiscard]] auto CreateShaderModuleFromFile(VkDevice device, std::filesystem::path file_path) -> ShaderModule;

	[[nodiscard]] auto GetPipelineStageVertexShaderCreateInfo(VkShaderModule shader_module, std::string_view name = "main") -> VkPipelineShaderStageCreateInfo;

	[[nodiscard]] auto GetPipelineStageFragmentShaderCreateInfo(VkShaderModule shader_module, std::string_view name = "main") -> VkPipelineShaderStageCreateInfo;
}