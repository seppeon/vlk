#pragma once
#include "vlk/VkHandles.hpp"
#include "vlk_meta/FnPtrTraits.hpp"
#include "vlk_meta/TL.hpp"
#include <cstddef>
#include <cstdint>
#include <Hz/Span.hpp>
#include <tuple>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	template <auto Ctor, auto Dtor>
	struct CmdTraits
	{
		using construct_args = FnPtrArgs<decltype(Ctor)>;
		using destruct_args = FnPtrArgs<decltype(Dtor)>;
		using common_indexes = TLCommonIs<0, construct_args, destruct_args>;
		using storage_type = TLCommon<construct_args, destruct_args>;
	};

	template <typename Args, typename Strs, typename Idxs, auto Ctor, auto Dtor>
	struct CmdBeginEndBase;
	template <typename ... Args, typename ... Strs, std::size_t ... Is, auto Ctor, auto Dtor>
	struct CmdBeginEndBase<TL<Args ...>, TL<Strs ...>, std::index_sequence<Is...>, Ctor, Dtor>
	{
		[[nodiscard]] auto operator()(Args ... args) const noexcept
		{
			return [=](auto fn)
			{
				Ctor(args...);
				fn();
				auto fwd = std::forward_as_tuple(args...);
				Dtor(std::get<Is>(fwd) ...);
			};
		}
	};

	template <auto Ctor, auto Dtor, typename Traits = CmdTraits<Ctor, Dtor>>
	struct CmdBeginEnd : CmdBeginEndBase<
			typename Traits::construct_args,
			typename Traits::storage_type,
			typename Traits::common_indexes,
			Ctor,
			Dtor
		>
	{
		using base = CmdBeginEndBase<
			typename Traits::construct_args,
			typename Traits::storage_type,
			typename Traits::common_indexes,
			Ctor,
			Dtor
		>;
		using base::operator();
	};

	[[nodiscard]] auto GetCommandPoolCreateInfo( std::uint32_t queue_family_index, bool commands_are_individually_rerecorded = false, bool commands_are_transient = false ) -> VkCommandPoolCreateInfo;

	[[nodiscard]] auto CreateCommandPool( VkDevice device, VkCommandPoolCreateInfo const & create_info ) -> CommandPool;

	[[nodiscard]] auto CreateCommandPool( VkDevice device, std::uint32_t queue_family_index, bool commands_are_individually_rerecorded = false, bool commands_are_transient = false ) -> CommandPool;

	[[nodiscard]] auto AllocateCommandBuffers( VkDevice device, VkCommandPool command_pool, bool is_primary, std::uint32_t command_buffer_count = 1 ) -> CommandBuffers;

	struct CommandBufferFlags
	{
		bool one_time_submit = false;
		bool render_pass_continue = false;
		bool simultaneous_use = false;
	};
	[[nodiscard]] bool BeginCommandBuffer( VkCommandBuffer command_buffer, CommandBufferFlags settings = {}, VkCommandBufferInheritanceInfo const * inheritance_info = nullptr );

	inline constexpr auto default_clear_values = std::array{ VkClearValue{ .color = { 0.0f, 0.0f, 0.0f, 1.0f } } };

	void CommandBeginRenderPassInline( VkCommandBuffer command_buffer, VkRenderPass render_pass, VkFramebuffer frame_buffer, VkRect2D render_area, Hz::Span<const VkClearValue> clear_values = default_clear_values);

	void CommandBeginRenderPassSecondaryCommandBuffers( VkCommandBuffer command_buffer, VkRenderPass render_pass, VkFramebuffer frame_buffer, VkRect2D render_area, Hz::Span<const VkClearValue> clear_values = default_clear_values);

	void CommandSetScissor(VkCommandBuffer command_buffer, Hz::Span<VkRect2D const> scissors, std::size_t first_scissor = 0);

	void CommandSetViewport(VkCommandBuffer command_buffer, Hz::Span<VkViewport const> viewports, std::size_t first_viewport = 0);

	void CommandBindGraphicsPipeline(VkCommandBuffer command_buffer, VkPipeline pipeline);

	void CommandBindComputePipeline(VkCommandBuffer command_buffer, VkPipeline pipeline);

	void CommandDraw(VkCommandBuffer command_buffer, std::size_t vertex_count, std::size_t instance_count, std::size_t first_vertex = 0, std::size_t first_instance = 0);

	void CommandDrawIndexed(VkCommandBuffer command_buffer, std::size_t vertex_count, std::size_t instance_count, std::size_t first_index = 0, std::size_t vertex_offset = 0, std::size_t first_instance = 0);

	void CommandEndRenderPass(VkCommandBuffer command_buffer);

	void EndCommandBuffer(VkCommandBuffer command_buffer);

	void ResetCommandBuffer(VkCommandBuffer command_buffer);

	void CommandBindU32IndexBuffers(VkCommandBuffer command_buffer, VkBuffer buffer, std::size_t offset = 0);

	void CommandBindVertexBuffers(VkCommandBuffer command_buffer, uint32_t first_binding, Hz::Span<VkBuffer const> buffers, Hz::Span<VkDeviceSize const> offsets);

	void CommandBindVertexBuffer(VkCommandBuffer command_buffer, uint32_t first_binding, Buffer const & buffer, std::size_t offset = 0);

	void CommandCopyBuffer(VkCommandBuffer command_buffer, VkBuffer src, VkBuffer dst, Hz::Span<VkBufferCopy const> regions);

	void CommandBindDescriptorSets(VkCommandBuffer commandBuffer, VkPipelineBindPoint pipeline_bind_point, VkPipelineLayout layout, uint32_t first_set, Hz::Span<const VkDescriptorSet> descriptor_sets, Hz::Span<const uint32_t> dynamic_offset = {});
}