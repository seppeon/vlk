#pragma once
#include "vlk/VkHandles.hpp"
#include <cstdint>
#include <Hz/Span.hpp>
#include <vulkan/vulkan.h>

namespace Vlk
{
	[[nodiscard]] auto GetBufferCreateInfo(std::size_t buffer_size, Hz::Span<std::uint32_t const> queue_family_indicies, VkBufferUsageFlags usage_flag) -> VkBufferCreateInfo;

	[[nodiscard]] auto GetExclusiveBufferCreateInfo( std::size_t buffer_size ) -> VkBufferCreateInfo;

	[[nodiscard]] auto GetConcurrentBufferCreateInfo( std::size_t buffer_size, Hz::Span<std::uint32_t const> queue_family_indicies ) -> VkBufferCreateInfo;

	[[nodiscard]] auto CreateBuffer( VkDevice device, VkBufferCreateInfo const & create_info, VkAllocationCallbacks const * allocator = nullptr ) -> Buffer;

	[[nodiscard]] auto CreateBuffer( VkDevice device, std::size_t buffer_size, Hz::Span<std::uint32_t const> queue_family_indicies, VkBufferUsageFlags usage_flag, VkAllocationCallbacks const * allocator = nullptr ) -> Buffer;

	[[nodiscard]] auto GetBufferMemoryRequirements( VkDevice device, VkBuffer buffer ) -> VkMemoryRequirements;

	inline constexpr auto no_supported_memory = ~std::size_t{};

	[[nodiscard]] auto GetSupportedMemory(std::uint32_t type_mask, VkPhysicalDeviceMemoryProperties const & supported_mem_properties, VkMemoryPropertyFlags mem_properties) -> std::size_t;
}