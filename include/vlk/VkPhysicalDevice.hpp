#pragma once
#include <vector>
#include <string>
#include <set>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
    [[nodiscard]] auto GetPhysicalDevices(VkInstance instance) -> std::vector<VkPhysicalDevice>;   

    [[nodiscard]] auto GetPhysicalDeviceSupportedExtensionNames(VkPhysicalDevice device) -> std::set<std::string>;

    [[nodiscard]] auto GetPhysicalDeviceSurfaceCapabilities(VkPhysicalDevice device, VkSurfaceKHR surface) -> VkSurfaceCapabilitiesKHR;

    [[nodiscard]] auto GetPhysicalDeviceSurfaceFormats(VkPhysicalDevice device, VkSurfaceKHR surface) -> std::vector<VkSurfaceFormatKHR>;

    [[nodiscard]] auto GetPhysicalDeviceSurfacePresentModes(VkPhysicalDevice device, VkSurfaceKHR surface) -> std::vector<VkPresentModeKHR>;

    struct SwapChainSupportDetails
    {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> present_modes;

        SwapChainSupportDetails(VkPhysicalDevice device, VkSurfaceKHR surface);

        [[nodiscard]] bool HasSurfaceFormat(VkSurfaceFormatKHR tested_format) const noexcept;

        [[nodiscard]] bool HasPresentMode(VkPresentModeKHR tested_present_mode) const noexcept;
    };

	[[nodiscard]] auto GetDeviceMemoryProperties(VkPhysicalDevice physical_device) -> VkPhysicalDeviceMemoryProperties;
}