#pragma once
#include <Hz/Span.hpp>
#include <cstdint>
#include <vulkan/vulkan_core.h>
#include <Hz/NSpan.hpp>

namespace Vlk
{
	void QueueSubmit(VkQueue queue, Hz::Span<VkSubmitInfo const> submit_infos, VkFence signalled_fence);

	void QueuePresent(VkQueue queue, Hz::Span<VkSemaphore const> wait_semaphores, Hz::Span<VkSwapchainKHR const> swap_chains, Hz::Span<std::uint32_t const> swap_indicies, Hz::Span<VkResult> results = {});
}