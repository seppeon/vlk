#pragma once
#include <set>
#include <string>
#include <vector>
#include <Hz/Span.hpp>
#include <exception>
#include <type_traits>

namespace Vlk
{
    [[nodiscard]] inline auto ToCStrVector(Hz::Span<std::string const> items) -> std::vector<const char *>
    {
        std::vector<const char *> output;
        for (auto const & item : items) output.push_back(item.data());
        return output;
    }

    [[nodiscard]] inline auto ToCStrVector(std::set<std::string> const & items) -> std::vector<const char *>
    {
        std::vector<const char *> output;
        for (auto const & item : items) output.push_back(item.data());
        return output;
    }

    template <class T, class Member>
        requires( std::is_trivially_default_constructible_v<T> )
    [[nodiscard]] consteval auto OffsetOfDataMember(Member T::*member_pointer) -> std::size_t {
        union { T object; char buffer[sizeof(T)]; }
        data{.object = T{}};
        T const& obj_ref = data.object;
        void const* mem_ptr = &(obj_ref.*member_pointer);
        for (std::size_t i = 0; i < sizeof(T); ++i) {
            void const* buffer_ptr = std::data(data.buffer) + i;
            if (buffer_ptr == mem_ptr) {
                return i;
            }
        }
        std::terminate();
    }

	template <typename T, std::size_t ... Ns>
	[[nodiscard]] constexpr auto ConcatArrays(std::array<T, Ns> const & ... arrays)
	{
		constexpr auto len = (Ns + ...);
		std::array<T, len> output;
		std::size_t i = 0;
		auto concat = [&](auto const & array)
		{
			for (auto & elem : array)
			{
				output[i++] = elem;
			}
		};
		(concat(arrays), ...);
		return output;
	}
}