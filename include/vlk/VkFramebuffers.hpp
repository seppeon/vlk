#pragma once
#include "vlk/VkHandles.hpp"
#include <cstdint>
#include <Hz/Span.hpp>
#include <vector>
#include <vulkan/vulkan.h>

namespace Vlk
{
	[[nodiscard]] auto GetFrameBufferCreateInfo(VkRenderPass render_pass, Hz::Span<VkImageView const> attachments, VkExtent2D extents, std::uint32_t layout_count) -> VkFramebufferCreateInfo;

	[[nodiscard]] auto CreateFrameBuffer(VkDevice device, VkFramebufferCreateInfo const & create_info) -> FrameBuffer;

	[[nodiscard]] auto CreateFrameBuffers(VkDevice device, Hz::Span<VkFramebufferCreateInfo const> create_infos) -> std::vector<FrameBuffer>;

	[[nodiscard]] auto CreateFrameBuffers(VkDevice device, VkRenderPass render_pass, VkExtent2D const & swap_extents, Hz::Span<ImageView const> image_views) -> std::vector<FrameBuffer>;
}