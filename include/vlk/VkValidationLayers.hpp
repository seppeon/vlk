#pragma once
#include "vlk/VkHandles.hpp"
#include "vlk_meta/RefOf.hpp"
#include <memory>
#include <set>
#include <string>
#include <type_traits>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
    [[nodiscard]] auto GetLayersCount() -> std::size_t;

    [[nodiscard]] auto GetSupportedLayers(std::size_t layer_count = GetLayersCount()) -> std::set<std::string>;

    template <typename T>
    concept DebugCallable = ConstLRef<T> and requires(T obj, std::string_view msg)
    {
        { obj(msg) } -> std::same_as<void>;
    };

    struct DebugCallback
    {
        DebugCallback() = delete;
        DebugCallback(DebugCallback &&) = delete;
        DebugCallback(DebugCallback const &) = delete;
        DebugCallback& operator=(DebugCallback &&) = delete;
        DebugCallback& operator=(DebugCallback const &) = delete;

        template <DebugCallable Obj>
        constexpr DebugCallback(Obj && obj)
            : ptr(std::addressof(obj))
            , callback(+[](void const * ptr, std::string_view msg) { (*static_cast<std::remove_reference_t<Obj> const *>(ptr))(msg); })
        {}

        constexpr void operator()(std::string_view msg) const noexcept
        {
            callback(ptr, msg);
        }
    private:
        void const * ptr;
        void (*callback)(void const *, std::string_view);
    };

    struct DebugUtilsSettings
    {
        bool enable_category_verbose = false;
        bool enable_category_info = false;
        bool enable_category_warning = true;
        bool enable_category_error = true;
        bool enable_type_general = true;
        bool enable_type_validation = true;
        bool enable_type_performance = true;
        bool enable_type_device_address_binding = false;
    };

    namespace Impl
    {
        [[nodiscard]] auto GetDebugUtilsMessengerCreateInfo(DebugUtilsSettings settings, DebugCallback const & callback) -> VkDebugUtilsMessengerCreateInfoEXT;
    }

    [[nodiscard]] auto GetDebugUtilsMessengerCreateInfo(DebugUtilsSettings settings, LRefOf<DebugCallback const> auto && callback) -> VkDebugUtilsMessengerCreateInfoEXT
    {
        return Impl::GetDebugUtilsMessengerCreateInfo(settings, callback);
    }

    [[nodiscard]] auto CreateDebugUtilsMessenger(VkInstance instance, VkDebugUtilsMessengerCreateInfoEXT const & create_info, VkAllocationCallbacks const * allocators = nullptr) -> DebugUtilsMessengerEXT;

    [[nodiscard]] auto CreateDebugUtilsMessenger(VkInstance instance, DebugUtilsSettings settings, LRefOf<DebugCallback const> auto && callback, VkAllocationCallbacks const * allocators = nullptr) -> DebugUtilsMessengerEXT
    {
        auto const create_infos = Impl::GetDebugUtilsMessengerCreateInfo(settings, callback);
        return Vlk::CreateDebugUtilsMessenger(instance, create_infos, allocators); 
    }
}