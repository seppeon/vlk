#pragma once
#include <vector>
#include <vulkan/vulkan.h>

namespace Vlk
{
	[[nodiscard]] auto GetPhysicalDeviceQueueFamilies(VkPhysicalDevice physical_device) -> std::vector<VkQueueFamilyProperties>;
}