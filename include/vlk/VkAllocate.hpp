#pragma once
#include <cstddef>
#include <vulkan/vulkan.h>

namespace Vlk
{
	struct MappedMemory
	{
		MappedMemory() = delete;
		MappedMemory(MappedMemory const & mapped_memory) = delete;
		MappedMemory& operator=(MappedMemory const & mapped_memory) = delete;
		MappedMemory(VkDevice device, VkDeviceMemory memory, VkDeviceSize offset, VkDeviceSize length, VkMemoryMapFlags flags);
		MappedMemory(MappedMemory && mapped_memory);
		MappedMemory& operator=(MappedMemory && mapped_memory);
		~MappedMemory();

		[[nodiscard]] auto Get() const noexcept -> void *;
	private:
		VkDevice m_device = {};
		VkDeviceMemory m_memory = {};
		void * m_ptr = nullptr;
	};

	struct DeviceMemory
	{
		DeviceMemory() = delete;
		DeviceMemory(DeviceMemory const & device_memory) = delete;
		DeviceMemory& operator=(DeviceMemory const & device_memory) = delete;
		DeviceMemory(VkDevice device, std::size_t alloc_size, std::size_t type_index, VkAllocationCallbacks const * allocator = nullptr);
		DeviceMemory(DeviceMemory && device_memory);
		DeviceMemory& operator=(DeviceMemory && device_memory);
		~DeviceMemory();

		[[nodiscard]] auto MapMemory(std::size_t offset, std::size_t length, VkMemoryMapFlags flags = {}) const noexcept -> MappedMemory;
	private:
		friend auto BindBuffer(DeviceMemory const & device_memory, VkBuffer buffer, std::size_t offset) noexcept -> VkResult;
		friend auto BindImage(DeviceMemory const & device_memory, VkImage image, std::size_t offset) noexcept -> VkResult;

		VkDevice m_device = {};
		VkDeviceMemory m_memory = {};
		VkAllocationCallbacks const * m_allocator = {};
	};

	[[nodiscard]] auto BindBuffer(DeviceMemory const & device_memory, VkBuffer buffer, std::size_t offset) noexcept -> VkResult;
	[[nodiscard]] auto BindImage(DeviceMemory const & device_memory, VkImage image, std::size_t offset) noexcept -> VkResult;
}