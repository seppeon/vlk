#pragma once
#include "vlk/VkHandles.hpp"
#include "vlk/VkSync.hpp"
#include <vector>
#include <vulkan/vulkan.h>
#include <cstdint>
#include <Hz/Span.hpp>

namespace Vlk
{
	[[nodiscard]] auto CreateSwapChain(
			VkDevice device,
			VkSurfaceKHR surface,
			Hz::Span<std::uint32_t const> queue_families,
			VkSurfaceCapabilitiesKHR capibilities,
			VkSurfaceFormatKHR surface_format,
			VkPresentModeKHR present_mode,
			VkExtent2D extent,
			std::uint32_t image_count
		) -> SwapChain;

	[[nodiscard]] auto CreateSwapchainImages(
			VkDevice device,
			VkSwapchainKHR swap_chain
		) -> std::vector<VkImage>;

	[[nodiscard]] auto CreateSwapchainImageViews(
			VkDevice device,
			Hz::Span<VkImage const> images,
			VkFormat swap_chain_format
		) -> std::vector<ImageView>;
}