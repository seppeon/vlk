#pragma once
#include "vlk/VkHandles.hpp"
#include <cstdint>
#include <Hz/Span.hpp>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	[[nodiscard]] auto GetPipelineInputAssemblyCreateInfo( VkPrimitiveTopology topology, bool primitive_restart_enable = false ) -> VkPipelineInputAssemblyStateCreateInfo;

	[[nodiscard]] auto GetPipelineVertexInputCreateInfo( Hz::Span<VkVertexInputBindingDescription const> vertex_binding_descriptions, Hz::Span<VkVertexInputAttributeDescription const> vertex_attribute_descriptions ) -> VkPipelineVertexInputStateCreateInfo;

	struct CullMode
	{
		bool cull_backface = false;
		bool cull_frontface = false;
		bool clockwise = true;
	};

	struct DepthBias
	{
		bool enabled = false;
		float constant_factor = 0.f;
		float clamp = 0.f;
		float slope_factor = 0.f;
	};

	struct RasterizerCreateSettings
	{
		bool discard_rasterizer_output = false;
		bool depth_clamp_enable = false;
		float line_width = 1.f;
		CullMode cull_mode = {};
		DepthBias depth_bias = {};
	};

	[[nodiscard]] auto GetFillRasterizerCreateInfo( RasterizerCreateSettings create_settings = {} ) -> VkPipelineRasterizationStateCreateInfo;

	[[nodiscard]] auto GetDisabledMultisampleCreateInfo() -> VkPipelineMultisampleStateCreateInfo;

	[[nodiscard]] auto GetDisabledDepthStencilCreateInfo() -> VkPipelineDepthStencilStateCreateInfo;

	[[nodiscard]] auto GetDisabledBlendAttachment() -> VkPipelineColorBlendAttachmentState;

	[[nodiscard]] auto GetAlphaBlendAttachment() -> VkPipelineColorBlendAttachmentState;

	struct BlendConstants
	{
		float r = 0.f;
		float g = 0.f;
		float b = 0.f;
		float a = 0.f;
	};

	[[nodiscard]] auto GetBlendCreateInfo( Hz::Span<VkPipelineColorBlendAttachmentState const> blend_attachments, BlendConstants blend_constants = {} ) -> VkPipelineColorBlendStateCreateInfo;

	[[nodiscard]] auto GetPipelineLayoutCreateInfo(Hz::Span<VkDescriptorSetLayout const> layouts = {}, Hz::Span<VkPushConstantRange const> push_constants = {}) -> VkPipelineLayoutCreateInfo;

	[[nodiscard]] auto CreatePipelineLayout( VkDevice device, VkPipelineLayoutCreateInfo const & create_info ) -> PipelineLayout;

	[[nodiscard]] auto CreatePipelineLayout( VkDevice device, Hz::Span<VkDescriptorSetLayout const> layouts = {}, Hz::Span<VkPushConstantRange const> push_constants = {} ) -> PipelineLayout;

	[[nodiscard]] auto GetMinimalColorAttachmentDescription( VkFormat format ) -> VkAttachmentDescription;

	[[nodiscard]] auto GetAttachmentReference( std::uint32_t attachment, VkImageLayout layout ) -> VkAttachmentReference;

	struct SubpassDescriptionArgs
	{
		Hz::Span<VkAttachmentReference const> input_attachments = {};
		Hz::Span<VkAttachmentReference const> colour_attachments = {};
		Hz::Span<VkAttachmentReference const> resolve_attachments = {};
		Hz::Span<VkAttachmentReference const> depth_stencil_attachments = {};
		Hz::Span<std::uint32_t const> preserve_attachments = {};
	};
	[[nodiscard]] auto GetSubpassDescription(SubpassDescriptionArgs args) -> VkSubpassDescription;

	struct RenderPassArgs
	{
		Hz::Span<VkAttachmentDescription const> attachments = {};
		Hz::Span<VkSubpassDescription const> subpasses = {};
		Hz::Span<VkSubpassDependency const> dependencies = {};
	};
	[[nodiscard]] auto GetRenderPassCreateInfo(RenderPassArgs args) -> VkRenderPassCreateInfo;

	[[nodiscard]] auto CreateRenderPass(VkDevice device, VkRenderPassCreateInfo const & create_info) -> RenderPass;

	[[nodiscard]] auto CreateRenderPass(VkDevice device, RenderPassArgs args) -> RenderPass;

	struct PipelineArgs
	{
		Hz::Span<VkPipelineShaderStageCreateInfo const> stages = {};
		VkPipelineVertexInputStateCreateInfo const * vertex_input_state = nullptr;
		VkPipelineInputAssemblyStateCreateInfo const * input_assembly_state = nullptr;
		VkPipelineTessellationStateCreateInfo const * tessellation_state = nullptr;
		VkPipelineViewportStateCreateInfo const * viewport_state = nullptr;
		VkPipelineRasterizationStateCreateInfo const * rasterization_state = nullptr;
		VkPipelineMultisampleStateCreateInfo const * multisample_state = nullptr;
		VkPipelineDepthStencilStateCreateInfo const * depth_stencil_state = nullptr;
		VkPipelineColorBlendStateCreateInfo const * color_blend_state = nullptr;
		VkPipelineDynamicStateCreateInfo const * dynamic_state = nullptr;
		VkPipelineLayout layout = {};
		VkRenderPass render_pass = {};
		std::uint32_t subpass = 0;
	};
	[[nodiscard]] auto GetPipelineCreateInfo(PipelineArgs args) -> VkGraphicsPipelineCreateInfo;

	[[nodiscard]] auto CreatePipeline(VkDevice device, Hz::Span<VkGraphicsPipelineCreateInfo const> create_infos) -> Pipeline;
}