#pragma once
#include "vlk/VkHandles.hpp"
#include <Hz/Span.hpp>
#include <cstdint>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	[[nodiscard]] auto GetUniformCreateInfo(std::size_t descriptor_count = 1) -> VkDescriptorPoolCreateInfo;

	[[nodiscard]] auto CreateDescriptorSetLayout(VkDevice logical_device, VkDescriptorSetLayoutCreateInfo const & create_info, VkAllocationCallbacks const * allocator = nullptr) -> DescriptorSetLayout;

	[[nodiscard]] auto CreateDescriptorSetLayout(VkDevice logical_device, VkDescriptorSetLayoutCreateFlags set_layout_create_flags, Hz::Span<VkDescriptorSetLayoutBinding const> bindings, VkAllocationCallbacks const * allocator = nullptr) -> DescriptorSetLayout;

	[[nodiscard]] auto CreateDescriptorPool(VkDevice logical_device, VkDescriptorPoolCreateInfo const & create_info, VkAllocationCallbacks const * allocator = nullptr) -> DescriptorPool;

	[[nodiscard]] auto CreateDescriptorPool(VkDevice logical_device, VkDescriptorPoolCreateFlags pool_create_flags, std::uint32_t max_sets, Hz::Span<VkDescriptorPoolSize const> pool_sizes, const VkAllocationCallbacks * allocator = nullptr) -> DescriptorPool;

	[[nodiscard]] auto AllocateDescriptorSets(VkDevice logical_device, VkDescriptorSetAllocateInfo const & create_info, VkAllocationCallbacks const * allocator = nullptr) -> DescriptorSets;

	[[nodiscard]] auto AllocateDescriptorSets(VkDevice logical_device, VkDescriptorPool descriptor_pool, Hz::Span<VkDescriptorSetLayout const> set_layouts, const VkAllocationCallbacks * allocator = nullptr) -> DescriptorSets;

	[[nodiscard]] auto GetWriteUniformDescriptorSet(VkDescriptorSet descriptor_set, std::uint32_t binding, std::uint32_t array_element, Hz::Span<VkDescriptorBufferInfo const> buffer_infos) -> VkWriteDescriptorSet;

	void UpdateDescriptorSet(VkDevice logical_device, Hz::Span<VkWriteDescriptorSet const> descriptor_writes, Hz::Span<VkCopyDescriptorSet const> descriptor_copies);

	void UpdateDescriptorSet(VkDevice logical_device, Hz::Span<VkCopyDescriptorSet const> descriptor_copies);

	void UpdateDescriptorSet(VkDevice logical_device, Hz::Span<VkWriteDescriptorSet const> descriptor_writes);

	// Used with:
	// - vkCmdBindDescriptorSets(VkCommandBuffer commandBuffer, VkPipelineBindPoint pipelineBindPoint, VkPipelineLayout layout, uint32_t firstSet, uint32_t descriptorSetCount, const VkDescriptorSet *pDescriptorSets, uint32_t dynamicOffsetCount, const uint32_t *pDynamicOffsets);
}