#pragma once
#include "Vlk/VkVersion.hpp"
#include "Vlk/Versioned.hpp"
#include "vlk/VkHandles.hpp"
#include <FL/Semver.hpp>
#include <vulkan/vulkan.h>
#include <set>

namespace Vlk
{
    [[nodiscard]] auto AppInfo(Versioned const & app, Versioned const & engine, FL::Semver api_version = ApiVersion()) -> VkApplicationInfo;

    [[nodiscard]] auto CreateInstance(VkApplicationInfo const & app_info, std::set<std::string> const & required_layers, std::set<std::string> const & required_extensions) -> Instance;
}