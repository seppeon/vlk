#pragma once
#include "vlk/VkHandles.hpp"

#include <cassert>
#include <chrono>
#include <limits>
#include <Hz/Span.hpp>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>


namespace Vlk
{
	[[nodiscard]] auto GetSemaphoreCreateInfo() -> VkSemaphoreCreateInfo;

	[[nodiscard]] auto GetFenceCreateInfo( bool signaled ) -> VkFenceCreateInfo;

	[[nodiscard]] auto CreateSemaphore( VkDevice device, VkSemaphoreCreateInfo const & create_info = GetSemaphoreCreateInfo(), VkAllocationCallbacks const * allocators = nullptr )
		-> Semaphore;

	[[nodiscard]] auto CreateFence( VkDevice device, VkFenceCreateInfo const & create_info, VkAllocationCallbacks const * allocators = nullptr ) -> Fence;

	[[nodiscard]] auto CreateFence( VkDevice device, bool signaled, VkAllocationCallbacks const * allocators = nullptr ) -> Fence;

	inline constexpr auto max_timeout = std::chrono::nanoseconds::max();
	/**
	 * @brief Waits for ALL fence to be signalled, then resets the fence and returns.
	 *
	 * @param device The logical device.
	 * @param fences The fences to wait for.
	 * @param timeout The timeout in nanoseconds.
	 * @return true All fences were signaled.
	 * @return false An error or timeout occurred.
	 */
	[[nodiscard]] bool WaitForAllFences( VkDevice device, Hz::Span<VkFence const> fences, std::chrono::nanoseconds timeout = max_timeout );
	/**
	 * @brief Waits for ANY fence to be signalled, then resets the fence and returns.
	 *
	 * @param device The logical device.
	 * @param fences The fences to wait for.
	 * @param timeout The timeout in nanoseconds.
	 * @return true All fences were signaled.
	 * @return false An error or timeout occurred.
	 */
	[[nodiscard]] bool WaitForAnyFences( VkDevice device, Hz::Span<VkFence const> fences, std::chrono::nanoseconds timeout = max_timeout );

	[[nodiscard]] auto AcquireNextImage( VkDevice device, VkSwapchainKHR swap_chain, VkSemaphore signal_semaphore, VkFence signal_fence, std::chrono::nanoseconds timeout = max_timeout ) -> std::uint32_t;

	[[nodiscard]] auto AcquireNextImage( VkDevice device, VkSwapchainKHR swap_chain, VkSemaphore signal_semaphore, std::chrono::nanoseconds timeout = max_timeout ) -> std::uint32_t;

	[[nodiscard]] auto AcquireNextImage( VkDevice device, VkSwapchainKHR swap_chain, VkFence signal_fence, std::chrono::nanoseconds timeout = max_timeout ) -> std::uint32_t;

	[[nodiscard]] auto GetSubmitCreateInfo( Hz::Span<VkSemaphore const> wait_semaphores, Hz::Span<VkPipelineStageFlags const> wait_dst_stage_mask, CommandBuffers const & command_buffers, Hz::Span<VkSemaphore const> signal_semaphores ) -> VkSubmitInfo;
}