#pragma once
#include <Hz/Span.hpp>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

namespace Vlk
{
	[[nodiscard]] auto GetDynamicStateCreateInfo(Hz::Span<VkDynamicState const> dynamic_states) -> VkPipelineDynamicStateCreateInfo;
}