#pragma once
#include <cstddef>
#include <set>
#include <string>

namespace Vlk
{
    [[nodiscard]] auto GetExtensionCount() -> std::size_t;

    [[nodiscard]] auto GetSupportedExtensions(std::size_t extension_count = GetExtensionCount()) -> std::set<std::string>;

    [[nodiscard]] auto GetDebugUtilsExtension() -> std::string;
}