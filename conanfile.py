from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps


class vlkRecipe(ConanFile):
    name = "vlk"
    version = "0.1.0"
    package_type = "library"

    # Optional metadata
    license = "MIT"
    author = "David Ledger davidledger@live.com.au"
    url = "https://gitlab.com/seppeon/vlk.git"
    description = "Following the vulkan-tutorials"
    topics = ("vulkan-tutorials")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    default_options = {
        "shared": False,
        "fPIC": True,
        "sdl_image/*:with_libjpeg": False,
        "sdl_image/*:with_libtiff": False
    }

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*"

    def requirements(self):
        self.requires("sdl/2.28.3", force=True)
        self.requires("glm/cci.20230113")
        self.requires("sdl_image/2.0.5")
        self.requires("file_list/0.2.3")
        self.requires("nspan/0.2.0")
        self.requires("concurrentqueue/1.0.4")
        self.requires("vulkan-loader/1.3.243.0")
        self.requires("vulkan-validationlayers/1.3.243.0")

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def layout(self):
        cmake_layout(self)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["vlk"]
