#include "vlk/Vlk.hpp"
#include "sdl2/Sdl2.hpp"
#include "vlk_meta/VlkMeta.hpp"
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vulkan/vulkan.h>

namespace
{
	struct ManagedBuffer
	{
		Vlk::Buffer buffer;
		VkMemoryRequirements memory_requirements = {};
		std::size_t transfer_memory_index = {};
		Vlk::DeviceMemory device_memory;

		ManagedBuffer(VkDevice logical_device, VkPhysicalDeviceMemoryProperties physical_device_memory_properties, std::size_t buffer_length, Hz::Span<const std::uint32_t> queue_family_indicies, VkBufferUsageFlags usage_flag, VkMemoryPropertyFlags memory_flags)
			: buffer( Vlk::CreateBuffer(logical_device, buffer_length, queue_family_indicies, usage_flag) )
			, memory_requirements(Vlk::GetBufferMemoryRequirements(logical_device, buffer))
			, transfer_memory_index(Vlk::GetSupportedMemory(memory_requirements.memoryTypeBits, physical_device_memory_properties, memory_flags))
			, device_memory(Vlk::DeviceMemory(logical_device, memory_requirements.size, transfer_memory_index))
		{
			if (BindBuffer(device_memory, buffer, 0) != VK_SUCCESS)
			{
				throw std::runtime_error("Could not create a managed buffer.");
			}
		}

		[[nodiscard]] auto MapMemory() const -> Vlk::MappedMemory
		{
			return device_memory.MapMemory(0, memory_requirements.size);
		}
	};

	struct Vertex
	{
		glm::vec2 pos;
		glm::vec3 color;
	};

	struct Ubo
	{
		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 projection;
	};

	template <std::size_t N>
	[[nodiscard]] constexpr auto Scale(std::array<Vertex, N> input, float scale) -> std::array<Vertex, N>
	{
		for (auto & [pos, _] : input)
		{
			pos.x *= scale;
			pos.y *= scale;
		}
		return input;
	}

	constexpr auto enable_validation_layers = bool(true);
	constexpr auto enable_verbose_event_log = bool(true);
	constexpr auto frames_in_flight = size_t(3);
}

int main() try
{
	auto const app_version = Vlk::Versioned{ .name = "tutorial 1" };
	auto const engine_version = Vlk::Versioned{ .name = "vulkan-tutorials" };
	auto required_physical_device_extensions = std::set<std::string>{ VK_KHR_SWAPCHAIN_EXTENSION_NAME };
	auto const dynamic_states = std::array{
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR,
	};
	auto const queue_priority = 1.0f;
	auto const vert_file = std::string( "vert.spv" );
	auto const frag_file = std::string( "frag.spv" );
	auto const icon_file = std::string( "tree.png" );
	auto window = Sdl2::Sdl2Window{
		app_version.name,
		{
			.x = Sdl2::Centered{},
			.y = Sdl2::Centered{},
			.width = 800,
			.height = 600
		},
		{
			.type = Sdl2::WindowType::Normal,
			.flags = {
				.resizable = false,
			},
			.graphics = {
				.vulkan = true,
			}
		}
	};
	auto const primitive_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	auto const vertices = Scale(
		std::array{
			Vertex{
				.pos{ -1, 1 },
				.color{ 1.0, 0.0, 0.0 },
			},
			Vertex{
				.pos{ 1, 1 },
				.color{ 0.0, 1.0, 0.0 },
			},
			Vertex{
				.pos{ 1, -1 },
				.color{ 0.0, 0.0, 1.0 },
			},
			Vertex{
				.pos{ -1, -1 },
				.color{ 1.0, 0.0, 1.0 },
			}
		},
		0.5
	);
	auto const vertices_indexes = std::array{
		std::uint32_t(0),
		std::uint32_t(1),
		std::uint32_t(2),
		std::uint32_t(2),
		std::uint32_t(3),
		std::uint32_t(0),
	};
	using vertex_description = Vlk::VkVertexDescription<0, VK_VERTEX_INPUT_RATE_VERTEX, &Vertex::pos, &Vertex::color>;

	auto const debug_callback = []( std::string_view msg ) { std::cerr.write( msg.data(), msg.size() ) << "\n"; };

	window.SetWindowIcon( icon_file );
	auto drawable_size = window.GetDrawableSize();
	auto required_extensions = window.GetVulkanExtensions();
	if ( enable_validation_layers )
	{
		required_extensions.insert( VK_EXT_DEBUG_UTILS_EXTENSION_NAME );
	}
	auto const supported_extensions = Vlk::GetSupportedExtensions();
	auto const has_required_extensions = std::ranges::includes( supported_extensions, required_extensions );
	if ( not has_required_extensions )
	{
		throw std::runtime_error( "System does not contain the required extensions." );
	}
	auto const supported_layers = Vlk::GetSupportedLayers();
	auto required_layers = std::set<std::string>{};
	if ( enable_validation_layers )
	{
		required_layers.insert( "VK_LAYER_KHRONOS_validation" );
	}
	auto const has_required_layers = std::ranges::includes( supported_layers, required_layers );
	if ( not has_required_layers )
	{
		throw std::runtime_error( "System does not contain the required layers." );
	}
	auto const app_info = Vlk::AppInfo( app_version, engine_version );
	auto const instance = Vlk::CreateInstance( app_info, required_layers, required_extensions );
	auto const debug_callback_container = Vlk::DebugCallback( debug_callback );
	std::optional<Vlk::DebugUtilsMessengerEXT> opt_debug_utils;
	if ( enable_validation_layers )
	{
		opt_debug_utils.emplace( Vlk::CreateDebugUtilsMessenger( instance, {}, debug_callback_container ) );
	}
	auto const surface = window.GetVulkanSurface( instance );
	auto const physical_devices = Vlk::GetPhysicalDevices( instance );
	auto const physical_device_ptr = Vlk::PickPhysicalDevice( physical_devices );
	if ( not physical_device_ptr )
	{
		throw std::runtime_error( "System does not have a physical device capible of running application." );
	}
	auto const & physical_device = *physical_device_ptr;
	auto physical_device_extensions = Vlk::GetPhysicalDeviceSupportedExtensionNames( physical_device );
	auto const has_required_physical_device_extensions = std::ranges::includes( physical_device_extensions, required_physical_device_extensions );
	if ( not has_required_physical_device_extensions )
	{
		throw std::runtime_error( "Selected physical device (graphics card) does not support the required extensions." );
	}
	auto const queue_families = Vlk::GetPhysicalDeviceQueueFamilies( physical_device );
	auto const picked_queue_families = Vlk::PickQueueFamilies( physical_device, surface, queue_families );
	auto const queue_support = Vlk::SwapChainSupportDetails( physical_device, surface );
	auto const present_mode = Vlk::PickPresentMode( queue_support );
	auto const surface_format = Vlk::PickSurfaceFormat( queue_support );
	auto const swap_extents = Vlk::PickSwapExtents( queue_support, drawable_size.width, drawable_size.height );
	auto const image_count = Vlk::PickImageCount( queue_support );
	if ( not( picked_queue_families.graphics_family and picked_queue_families.present_family ) )
	{
		throw std::runtime_error( "The physical device does not have queues that support the requested families." );
	}
	auto const picked_queues = std::array{ picked_queue_families.graphics_family.value(), picked_queue_families.present_family.value() };
	auto const unique_queues = Vlk::GetUniqueQueues( picked_queues );
	auto const logical_device = Vlk::CreateLogicalDevice( physical_device, required_physical_device_extensions, unique_queues, queue_priority );
	auto const swap_chain = Vlk::CreateSwapChain( logical_device, surface, unique_queues, queue_support.capabilities, surface_format, present_mode, swap_extents, image_count );
	auto const swap_chain_images = Vlk::CreateSwapchainImages( logical_device, swap_chain );
	auto const swap_chain_image_views = Vlk::CreateSwapchainImageViews( logical_device, swap_chain_images, surface_format.format );
	auto const vert_shader_module = Vlk::CreateShaderModuleFromFile( logical_device, vert_file );
	auto const frag_shader_module = Vlk::CreateShaderModuleFromFile( logical_device, frag_file );
	auto const shader_stages = std::array{ Vlk::GetPipelineStageVertexShaderCreateInfo( vert_shader_module ), Vlk::GetPipelineStageFragmentShaderCreateInfo( frag_shader_module ) };
	auto const dynamic_states_create_info = Vlk::GetDynamicStateCreateInfo( dynamic_states );
	auto const vertex_binding_description = std::array{ vertex_description::binding_description };
	auto const vertex_attribute_description = vertex_description::attribute_descriptions;
	auto const vertex_input_create_info = Vlk::GetPipelineVertexInputCreateInfo( vertex_binding_description, vertex_attribute_description );
	auto const vertex_input_assembly_create_info = Vlk::GetPipelineInputAssemblyCreateInfo( primitive_topology );
	auto const viewport = std::array{ Vlk::GetViewport( 0, 0, swap_extents.width, swap_extents.height ) };
	auto const scissor = std::array{ Vlk::GetScissor( 0, 0, swap_extents.width, swap_extents.height ) };
	auto const viewport_create_info = Vlk::GetViewportCreateInfo( viewport, scissor );
	auto const rasterizer_create_info = Vlk::GetFillRasterizerCreateInfo();
	auto const multisampling_create_info = Vlk::GetDisabledMultisampleCreateInfo();
	auto const depth_stencil_create_info = Vlk::GetDisabledDepthStencilCreateInfo();
	auto const blend_attachment = std::array{ Vlk::GetDisabledBlendAttachment() };
	auto const blend_create_info = Vlk::GetBlendCreateInfo( blend_attachment );
	auto const colour_attachments = std::array{ Vlk::GetMinimalColorAttachmentDescription( surface_format.format ) };
	auto const colour_attachment_ref = std::array{ Vlk::GetAttachmentReference( 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL ) };
	auto const subpass_description = std::array{ Vlk::GetSubpassDescription( { .colour_attachments = { colour_attachment_ref } } ) };
	auto const subpass_dependancies = std::array{
		VkSubpassDependency{
			.srcSubpass = VK_SUBPASS_EXTERNAL,
			.dstSubpass = 0,
			.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 
			.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			.srcAccessMask = VkAccessFlagBits{}, // Nothing needed, we are starting from clear.
			.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
			.dependencyFlags = {},
		}
	};
	auto const render_pass_args = Vlk::RenderPassArgs{
		.attachments = { colour_attachments },
		.subpasses = { subpass_description },
		.dependencies = { subpass_dependancies },
	};

	auto const render_pass = Vlk::CreateRenderPass( logical_device, render_pass_args );
	auto const graphics_queue = Vlk::GetDeviceGraphicsQueue( logical_device, picked_queue_families );
	auto const present_queue = Vlk::GetDevicePresentQueue( logical_device, picked_queue_families );
	auto const frame_buffers = Vlk::CreateFrameBuffers( logical_device, render_pass, swap_extents, swap_chain_image_views );
	auto const command_pool = Vlk::CreateCommandPool( logical_device, *picked_queue_families.graphics_family, true );
	auto const physical_device_memory_properties = Vlk::GetDeviceMemoryProperties(physical_device);

	auto const indexes_buffer = ManagedBuffer(
		logical_device, physical_device_memory_properties,
		Vlk::SizeOf(vertices_indexes),
		{}, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
	);
	auto const vertexes_buffer = ManagedBuffer(
		logical_device, physical_device_memory_properties,
		Vlk::SizeOf(vertices),
		{}, VK_BUFFER_USAGE_TRANSFER_DST_BIT |VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
	);
	auto const transfer_buffer = ManagedBuffer(
		logical_device, physical_device_memory_properties,
		Vlk::SizeOf(vertices) + Vlk::SizeOf(vertices_indexes),
		{}, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
	);
	auto const uniform_buffer = ManagedBuffer(
		logical_device, physical_device_memory_properties,
		sizeof(Ubo) * frames_in_flight,
		{}, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
	);

	auto const mapped_memory = uniform_buffer.MapMemory();
	auto mapped_uniform_ptr = static_cast<char *>(mapped_memory.Get());
	auto mapped_ubos = std::array<char *, frames_in_flight>{};
	for (auto & ptr : mapped_ubos)
	{ 
		ptr = mapped_uniform_ptr;
		mapped_uniform_ptr += sizeof(Ubo);
	};

	auto const pool_sizes = std::array{VkDescriptorPoolSize{
		.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.descriptorCount = frames_in_flight,
	}};
	auto const descriptor_pool = Vlk::CreateDescriptorPool(logical_device, VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT, frames_in_flight, pool_sizes);

	auto const descriptor_set_bindings = std::array{
			VkDescriptorSetLayoutBinding{
			.binding = 0,
			.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
			.pImmutableSamplers = nullptr,
		}
	};
	auto const pipeline_layouts = Vlk::CreateDescriptorSetLayout(
		logical_device,
		VkDescriptorSetLayoutCreateFlags{},
		descriptor_set_bindings
	);
	auto const set_layouts = std::array{ pipeline_layouts.Get() };
	auto const descriptor_sets = Vlk::AllocateDescriptorSets(logical_device, descriptor_pool, set_layouts);
	for (std::size_t i = 0; i < descriptor_sets.size(); ++i)
	{
		auto const buffer_infos = std::array{
			VkDescriptorBufferInfo{
				.buffer = uniform_buffer.buffer,
				.offset = i * sizeof(Ubo),
				.range = sizeof(Ubo),
			}
		};
		auto const write_descriptors = std::array{ Vlk::GetWriteUniformDescriptorSet(descriptor_sets[i], 0, 0, buffer_infos) };
		Vlk::UpdateDescriptorSet(logical_device, write_descriptors);
	}

	auto const pipeline_layout = Vlk::CreatePipelineLayout(logical_device, set_layouts);
	auto const pipeline_create_info = std::array{
		Vlk::GetPipelineCreateInfo( Vlk::PipelineArgs{
			.stages = { shader_stages },
			.vertex_input_state = &vertex_input_create_info,
			.input_assembly_state = &vertex_input_assembly_create_info,
			.tessellation_state = nullptr,
			.viewport_state = &viewport_create_info,
			.rasterization_state = &rasterizer_create_info,
			.multisample_state = &multisampling_create_info,
			.depth_stencil_state = &depth_stencil_create_info,
			.color_blend_state = &blend_create_info,
			.dynamic_state = &dynamic_states_create_info,
			.layout = pipeline_layout,
			.render_pass = render_pass,
			.subpass = 0,
		} )
	};
	auto const graphics_pipeline = Vlk::CreatePipeline( logical_device, pipeline_create_info );
	auto const transfer_command_buffers = Vlk::AllocateCommandBuffers( logical_device, command_pool, true );
	auto const & transfer_command_buffer = transfer_command_buffers.front();
	{
		auto const transfer_mapped_vertexes = transfer_buffer.MapMemory();
		auto * dst = static_cast<char *>(transfer_mapped_vertexes.Get());
		std::memcpy(dst, vertices.data(), Vlk::SizeOf(vertices));
		dst += Vlk::SizeOf(vertices);
		std::memcpy(dst, vertices_indexes.data(), Vlk::SizeOf(vertices_indexes));

		// Copy verticies onto the device.
		Vlk::ResetCommandBuffer( transfer_command_buffer );
		if ( Vlk::BeginCommandBuffer( transfer_command_buffer, { .one_time_submit = true } ) )
		{
			Vlk::CommandCopyBuffer(
				transfer_command_buffer,
				transfer_buffer.buffer,
				vertexes_buffer.buffer,
				std::array{
					VkBufferCopy{
						.srcOffset = 0,
						.dstOffset = 0,
						.size = Vlk::SizeOf(vertices),
					}
				}
			);
			Vlk::CommandCopyBuffer(
				transfer_command_buffer,
				transfer_buffer.buffer,
				indexes_buffer.buffer,
				std::array{
					VkBufferCopy{
						.srcOffset = Vlk::SizeOf(vertices),
						.dstOffset = 0,
						.size = Vlk::SizeOf(indexes_buffer.buffer),
					}
				}
			);
			Vlk::EndCommandBuffer(transfer_command_buffer);
		}
	}

	auto const image_avaliable_semaphore = Vlk::CreateSemaphore( logical_device );
	auto const render_finished_semaphore = Vlk::CreateSemaphore( logical_device );
	auto const inflight_fence = Vlk::CreateFence( logical_device, false );

	// Submit the command buffer, loading up the verticies on the local device memory
	auto const submit_infos = std::array{ Vlk::GetSubmitCreateInfo({}, {}, transfer_command_buffers, {}) };
	Vlk::QueueSubmit(graphics_queue.queue, submit_infos, inflight_fence);

	auto const graphics_command_buffers = Vlk::AllocateCommandBuffers( logical_device, command_pool, true );
	auto const & graphics_command_buffer = graphics_command_buffers.front();

	auto const start = std::chrono::system_clock::now();
	auto const time_since_start = [&]{
		return std::chrono::duration<float, std::chrono::seconds::period>(std::chrono::system_clock::now() - start).count();
	};
	auto const look_at_view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	auto const projection = glm::perspective(glm::radians(45.0f), static_cast<float>(swap_extents.width) / static_cast<float>(swap_extents.height), 0.1f, 10.0f);

	auto const record_command_buffer = [&]( std::uint32_t frame_index )
	{
		auto const ubo = Ubo{
			.model = glm::rotate(glm::mat4(1.f), time_since_start() * glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f)),
			.view = look_at_view,
			.projection = projection,
		};
		std::memcpy(mapped_ubos[frame_index], &ubo, sizeof(ubo));
		Vlk::ResetCommandBuffer(graphics_command_buffer);
		if ( Vlk::BeginCommandBuffer(graphics_command_buffer) )
		{
			Vlk::CommandBeginRenderPassInline(graphics_command_buffer, render_pass, frame_buffers[frame_index], VkRect2D{ {}, swap_extents });
			Vlk::CommandBindGraphicsPipeline(graphics_command_buffer, graphics_pipeline);
			Vlk::CommandSetViewport(graphics_command_buffer, viewport);
			Vlk::CommandSetScissor(graphics_command_buffer, scissor);
			Vlk::CommandBindVertexBuffer(graphics_command_buffer, 0, vertexes_buffer.buffer);
			Vlk::CommandBindU32IndexBuffers(graphics_command_buffer, indexes_buffer.buffer);
			Vlk::CommandBindDescriptorSets(
				graphics_command_buffer,
				VK_PIPELINE_BIND_POINT_GRAPHICS,
				pipeline_layout,
				0,
				descriptor_sets
			);
			Vlk::CommandDrawIndexed(graphics_command_buffer, vertices_indexes.size(), 1);
			Vlk::CommandEndRenderPass(graphics_command_buffer);
			Vlk::EndCommandBuffer(graphics_command_buffer);
		}
	};

	auto const draw_frame = [&]()
	{
		// Wait to be ready for next image.
		auto const inflight_fence_array = std::array{ VkFence( inflight_fence ) };
		if ( not Vlk::WaitForAllFences( logical_device, inflight_fence_array ) )
		{
			throw std::runtime_error( "Unable to wait for fences." );
		}

		// Get the next image.
		auto const next_frame_index = Vlk::AcquireNextImage( logical_device, swap_chain, image_avaliable_semaphore );
		if ( next_frame_index >= swap_chain_images.size() )
		{
			throw std::runtime_error( "Unable to acquire next image." );
		}
		record_command_buffer( next_frame_index );

		auto const wait_semaphores = std::array{ VkSemaphore( image_avaliable_semaphore ) };
		auto const wait_stages = std::array{ VkPipelineStageFlags( VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT ) };
		auto const signal_semaphores = std::array{ VkSemaphore( render_finished_semaphore ) };
		auto const swap_chains = std::array{ VkSwapchainKHR( swap_chain ) };
		auto const submit_infos = std::array{
			Vlk::GetSubmitCreateInfo(
				wait_semaphores,
				wait_stages, 
				graphics_command_buffers,
				signal_semaphores
			)
		};
		Vlk::QueueSubmit(graphics_queue.queue, submit_infos, inflight_fence);
		Vlk::QueuePresent(graphics_queue.queue, signal_semaphores, swap_chains, { 1, &next_frame_index });
	};

	int fps = 0;
	auto last_frame_measurement_time = std::chrono::system_clock::now();
	for (;;)
	{
		++fps;
		draw_frame();

		// Handle window events (otherwise this window will be quite bricked).
		Sdl2::PumpEvents();
		auto const events = Sdl2::PollEvents();

		// Print events recieved.
		if ( enable_verbose_event_log )
		{
			if (not events.empty())
			{
				std::string new_log;
				for ( auto const & event : events ) { new_log += Sdl2::DescribeEvent( event ) + "\n"; }
				std::cout.write( new_log.data(), new_log.size() );
			}
			auto const now = std::chrono::system_clock::now();
			if ((now - last_frame_measurement_time) >= std::chrono::seconds(1))
			{
				last_frame_measurement_time = now;
				std::cout << "fps: " << fps << "\n";
				fps = 0;
			}
		}

		// Close when close is requested. 
		if ( not Sdl2::DefaultEventHandler( events ) )
		{
			break;
		}
	}
	vkDeviceWaitIdle( logical_device );
	return 0;
}
catch ( std::exception const & e )
{
	std::cerr << "error: " << e.what() << std::endl;
	return -1;
}